use std::collections::HashSet;

struct Sensor {
    position: (i32, i32),
    beacon: (i32, i32),
}

impl Sensor {
    fn parse(line: &str) -> Self {
        assert!(line.starts_with("Sensor at x="));
        let comma_idx = line.find(',').unwrap();
        let sensor_x = line[12..comma_idx].parse().unwrap();
        let colon_idx = line.find(':').unwrap();
        let sensor_y = line[comma_idx + 4..colon_idx].parse().unwrap();

        assert_eq!(
            ": closest beacon is at x=",
            &line[colon_idx..colon_idx + 25]
        );

        let comma_idx = line[colon_idx + 25..].find(',').unwrap() + colon_idx + 25;
        let beacon_x = line[colon_idx + 25..comma_idx].parse().unwrap();
        let beacon_y = line[comma_idx + 4..].parse().unwrap();

        Self {
            position: (sensor_x, sensor_y),
            beacon: (beacon_x, beacon_y),
        }
    }
}

trait Distance {
    fn dist(&self, other: &(i32, i32)) -> i32;
}

impl Distance for (i32, i32) {
    fn dist(&self, other: &(i32, i32)) -> i32 {
        i32::abs(self.0 - other.0) + i32::abs(self.1 - other.1)
    }
}

#[derive(Clone)]
struct Interval {
    start: i32,
    end: i32,
}

impl Interval {
    fn new(start: i32, end: i32) -> Self {
        Self {
            start,
            end: end + 1,
        }
    }

    fn is_empty(&self) -> bool {
        self.start == self.end
    }

    fn overlaps(&self, other: &Interval) -> bool {
        if self.is_empty() || other.is_empty() {
            return true;
        }

        self.start <= other.end && self.end >= other.start
    }

    // Interval must overlap!
    fn union(&mut self, other: &Interval) {
        if other.is_empty() {
            return;
        }

        if self.is_empty() {
            self.start = other.start;
            self.end = other.end;
        }

        self.start = i32::min(self.start, other.start);
        self.end = i32::max(self.end, other.end);
    }

    fn len(&self) -> usize {
        if self.is_empty() {
            0
        } else {
            (self.end - self.start) as usize
        }
    }
}

struct CompoundInterval {
    intervals: Vec<Interval>,
}

impl CompoundInterval {
    fn new() -> Self {
        Self {
            intervals: Vec::new(),
        }
    }

    fn add_interval(&mut self, interval: Interval) {
        if interval.is_empty() {
            return;
        }

        for x in &mut self.intervals {
            if x.overlaps(&interval) {
                x.union(&interval);
                return;
            }
        }

        self.intervals.push(interval);
    }

    fn simplify(&mut self) {
        self.intervals.sort_by_key(|x| x.start);

        let mut fused = Vec::new();
        fused.push(self.intervals[0].clone());
        for interval in &self.intervals {
            let current = fused.last_mut().unwrap();
            if current.overlaps(&interval) {
                current.union(&interval);
            } else {
                fused.push(interval.clone());
            }
        }

        self.intervals = fused;
    }
}

fn covered_intervals(sensors: &[Sensor], y: i32, x_bounds: Option<(i32, i32)>) -> CompoundInterval {
    let mut intervals = CompoundInterval::new();
    for sensor in sensors {
        let distance_to_beacon = sensor.position.dist(&sensor.beacon);

        if i32::abs(sensor.position.1 - y) > distance_to_beacon {
            continue;
        }

        let offset = distance_to_beacon - i32::abs(sensor.position.1 - y);
        let (min, max) = {
            let min = i32::min(sensor.position.0 - offset, sensor.position.0 + offset);
            let max = i32::max(sensor.position.0 - offset, sensor.position.0 + offset);

            if let Some((min_x, max_x)) = x_bounds {
                (i32::max(min_x, min), i32::min(max_x, max))
            } else {
                (min, max)
            }
        };

        intervals.add_interval(Interval::new(min, max));
    }

    if intervals.intervals.len() > 1 {
        intervals.simplify();
    }

    intervals
}

fn count_unavailable(sensors: &[Sensor], y: i32) -> usize {
    let beacons: HashSet<(i32, i32)> = sensors
        .iter()
        .filter(|s| s.beacon.1 == y)
        .map(|s| s.beacon.clone())
        .collect();
    let intervals = covered_intervals(&sensors, y, None);
    assert_eq!(1, intervals.intervals.len());

    intervals.intervals[0].len() - beacons.len()
}

fn find_hidden_beacon(sensors: &[Sensor], grid_size: i32) -> Option<(i32, i32)> {
    for y in 0..grid_size {
        let intervals = covered_intervals(&sensors, y, Some((0, grid_size)));

        if intervals.intervals.len() == 1 {
            continue;
        } else if intervals.intervals.len() == 2 {
            return Some((intervals.intervals[0].end, y));
        } else {
            panic!("Found too many intervals!");
        }
    }

    None
}

fn main() {
    let lines = aoc_2022::input_lines();
    let sensors = lines
        .iter()
        .map(|line| Sensor::parse(line))
        .collect::<Vec<_>>();

    println!("01: {}", count_unavailable(&sensors, 2000000));

    let (x, y) = find_hidden_beacon(&sensors, 4000000).unwrap();
    let frequency = (x as i64) * 4000000 + (y as i64);
    println!("02: {}", frequency);
}

#[cfg(test)]
mod tests {
    use crate::{count_unavailable, find_hidden_beacon, Sensor};

    #[test]
    fn examples_part01() {
        let lines = [
            "Sensor at x=2, y=18: closest beacon is at x=-2, y=15",
            "Sensor at x=9, y=16: closest beacon is at x=10, y=16",
            "Sensor at x=13, y=2: closest beacon is at x=15, y=3",
            "Sensor at x=12, y=14: closest beacon is at x=10, y=16",
            "Sensor at x=10, y=20: closest beacon is at x=10, y=16",
            "Sensor at x=14, y=17: closest beacon is at x=10, y=16",
            "Sensor at x=8, y=7: closest beacon is at x=2, y=10",
            "Sensor at x=2, y=0: closest beacon is at x=2, y=10",
            "Sensor at x=0, y=11: closest beacon is at x=2, y=10",
            "Sensor at x=20, y=14: closest beacon is at x=25, y=17",
            "Sensor at x=17, y=20: closest beacon is at x=21, y=22",
            "Sensor at x=16, y=7: closest beacon is at x=15, y=3",
            "Sensor at x=14, y=3: closest beacon is at x=15, y=3",
            "Sensor at x=20, y=1: closest beacon is at x=15, y=3",
        ]
        .map(String::from);
        let sensors = lines
            .iter()
            .map(|line| Sensor::parse(line))
            .collect::<Vec<_>>();

        assert_eq!(26, count_unavailable(&sensors, 10));
    }

    #[test]
    fn examples_part02() {
        let lines = [
            "Sensor at x=2, y=18: closest beacon is at x=-2, y=15",
            "Sensor at x=9, y=16: closest beacon is at x=10, y=16",
            "Sensor at x=13, y=2: closest beacon is at x=15, y=3",
            "Sensor at x=12, y=14: closest beacon is at x=10, y=16",
            "Sensor at x=10, y=20: closest beacon is at x=10, y=16",
            "Sensor at x=14, y=17: closest beacon is at x=10, y=16",
            "Sensor at x=8, y=7: closest beacon is at x=2, y=10",
            "Sensor at x=2, y=0: closest beacon is at x=2, y=10",
            "Sensor at x=0, y=11: closest beacon is at x=2, y=10",
            "Sensor at x=20, y=14: closest beacon is at x=25, y=17",
            "Sensor at x=17, y=20: closest beacon is at x=21, y=22",
            "Sensor at x=16, y=7: closest beacon is at x=15, y=3",
            "Sensor at x=14, y=3: closest beacon is at x=15, y=3",
            "Sensor at x=20, y=1: closest beacon is at x=15, y=3",
        ]
        .map(String::from);
        let sensors = lines
            .iter()
            .map(|line| Sensor::parse(line))
            .collect::<Vec<_>>();

        let (x, y) = find_hidden_beacon(&sensors, 20).unwrap();
        assert_eq!(14, x);
        assert_eq!(11, y);
    }
}
