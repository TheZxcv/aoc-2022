use std::collections::VecDeque;

struct Climb {
    elevations: Vec<Vec<u32>>,
    width: usize,
    height: usize,
    start: (usize, usize),
    target: (usize, usize),
}

impl Climb {
    fn parse(lines: &[String]) -> Climb {
        let mut start = None;
        let mut target = None;
        let mut elevations = Vec::new();
        for (i, line) in lines.iter().enumerate() {
            elevations.push(Vec::new());
            for (j, ch) in line.chars().enumerate() {
                let elevation = match ch {
                    'S' => {
                        start = Some((i, j));
                        Self::elevation_value('a')
                    }
                    'E' => {
                        target = Some((i, j));
                        Self::elevation_value('z')
                    }
                    ch => Self::elevation_value(ch),
                };

                elevations.last_mut().unwrap().push(elevation);
            }
        }
        let width = elevations[0].len();
        let height = elevations.len();

        Climb {
            elevations,
            width,
            height,
            start: start.unwrap(),
            target: target.unwrap(),
        }
    }

    fn elevation_value(ch: char) -> u32 {
        match ch {
            'a'..='z' => (ch as u32) - ('a' as u32),
            _ => panic!("Invalid elevation value: {}", ch),
        }
    }

    fn min_path(&self) -> u32 {
        self.bfs(
            self.start,
            |climb, current, next| {
                climb.elevations[current.0][current.1] + 1 >= climb.elevations[next.0][next.1]
            },
            |climb, node| climb.target == node,
        )
        .unwrap()
    }

    fn best_starting_point_distance(&self) -> u32 {
        self.bfs(
            self.target,
            |climb, current, next| {
                climb.elevations[next.0][next.1] + 1 >= climb.elevations[current.0][current.1]
            },
            |climb: &Climb, (i, j)| climb.elevations[i][j] == 0,
        )
        .unwrap()
    }

    fn bfs(
        &self,
        start: (usize, usize),
        is_valid_next: fn(&Climb, (usize, usize), (usize, usize)) -> bool,
        is_target: fn(&Climb, (usize, usize)) -> bool,
    ) -> Option<u32> {
        let mut visited = vec![vec![false; self.width]; self.height];
        let mut frontier = VecDeque::new();

        frontier.push_back((0, start));

        while let Some((distance, current)) = frontier.pop_front() {
            if visited[current.0][current.1] {
                continue;
            }

            visited[current.0][current.1] = true;

            for (y_offset, x_offset) in [(-1, 0), (1, 0), (0, -1), (0, 1)] {
                let ii = (current.0 as i64) + y_offset;
                let jj = (current.1 as i64) + x_offset;
                if ii < 0 || jj < 0 {
                    continue;
                }

                let ii = ii as usize;
                let jj = jj as usize;
                if ii >= self.height || jj >= self.width || visited[ii][jj] {
                    continue;
                }

                if is_valid_next(&self, current, (ii, jj)) {
                    if is_target(self, (ii, jj)) {
                        return Some(distance + 1);
                    }

                    frontier.push_back((distance + 1, (ii, jj)));
                }
            }
        }

        None
    }
}

fn main() {
    let lines = aoc_2022::input_lines();
    let climb = Climb::parse(&lines);

    let min_distance = climb.min_path();
    println!("01: {}", min_distance);

    let distance = climb.best_starting_point_distance();
    println!("02: {}", distance);
}

#[cfg(test)]
mod tests {
    use crate::Climb;

    #[test]
    fn examples_part01() {
        let input = ["Sabqponm", "abcryxxl", "accszExk", "acctuvwj", "abdefghi"].map(String::from);
        let climb = Climb::parse(&input);

        assert_eq!(31, climb.min_path());
    }

    #[test]
    fn examples_part02() {
        let input = ["Sabqponm", "abcryxxl", "accszExk", "acctuvwj", "abdefghi"].map(String::from);
        let climb = Climb::parse(&input);

        assert_eq!(29, climb.best_starting_point_distance());
    }
}
