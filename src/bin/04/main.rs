pub struct Assignment {
    start: i32,
    end: i32,
}

impl Assignment {
    fn parse(s: &str) -> Self {
        if let Some((start, end)) = s.split_once('-') {
            return Self {
                start: start.parse().unwrap(),
                end: end.parse().unwrap(),
            };
        }

        panic!("Failed to parse range.")
    }

    pub fn contains(&self, other: &Assignment) -> bool {
        self.start <= other.start && self.end >= other.end
    }

    pub fn overlaps(&self, other: &Assignment) -> bool {
        self.start <= other.end && self.end >= other.start
    }
}

fn main() {
    let pairs = aoc_2022::input_lines()
        .iter()
        .filter_map(|line| line.split_once(','))
        .map(|(a, b)| (Assignment::parse(a), Assignment::parse(b)))
        .collect::<Vec<_>>();

    let contains_count = pairs
        .iter()
        .filter(|(a, b)| a.contains(b) || b.contains(a))
        .count();
    println!("01: {}", contains_count);

    let overlaps_count = pairs.iter().filter(|(a, b)| a.overlaps(b)).count();
    println!("02: {}", overlaps_count);
}
