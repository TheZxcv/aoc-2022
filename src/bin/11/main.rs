use std::{collections::VecDeque, iter::FromIterator};

#[derive(Clone, Copy)]
enum Operand {
    Int(u64),
    Old,
}

#[derive(Clone, Copy)]
enum Operator {
    Add,
    Mul,
}

#[derive(Clone)]
struct Monkey {
    items: VecDeque<u64>,
    update_expression: (Operand, Operator, Operand),
    test_constant: u64,
    destination_if_true: usize,
    destination_if_false: usize,
}

impl Monkey {
    fn parse(lines: &[String]) -> Self {
        assert!(lines[0].starts_with("Monkey "));

        assert!(lines[1].starts_with("  Starting items: "));
        let items = lines[1][18..]
            .split(',')
            .map(|e| e.trim().parse::<u64>().unwrap());

        assert!(lines[2].starts_with("  Operation: new = "));
        let update_expression = Self::parse_expression(&lines[2][19..]);

        assert!(lines[3].starts_with("  Test: divisible by "));
        let test_constant = lines[3][21..].parse().unwrap();

        assert!(lines[4].starts_with("    If true: throw to monkey "));
        let destination_if_true = lines[4][29..].parse().unwrap();

        assert!(lines[5].starts_with("    If false: throw to monkey "));
        let destination_if_false = lines[5][30..].parse().unwrap();

        Self {
            items: VecDeque::from_iter(items),
            update_expression,
            test_constant,
            destination_if_true,
            destination_if_false,
        }
    }

    fn parse_expression(s: &str) -> (Operand, Operator, Operand) {
        let mut splits = s.split_ascii_whitespace();
        let left = match splits.next() {
            Some("old") => Operand::Old,
            Some(number) => Operand::Int(number.parse().unwrap()),
            None => panic!("Missing operand."),
        };

        let op = match splits.next() {
            Some("+") => Operator::Add,
            Some("*") => Operator::Mul,
            Some(operator) => panic!("Unknown operator: {}.", operator),
            None => panic!("Missing operator."),
        };

        let right = match splits.next() {
            Some("old") => Operand::Old,
            Some(number) => Operand::Int(number.parse().unwrap()),
            None => panic!("Missing operand."),
        };

        (left, op, right)
    }

    fn do_turn(&mut self, relief: u64) -> Vec<(usize, u64)> {
        let mut items_thrown = Vec::new();
        while let Some(worry_level) = self.items.pop_back() {
            let new_worry_level = self.update_worry(worry_level) / relief;

            let destination = if new_worry_level % self.test_constant == 0 {
                self.destination_if_true
            } else {
                self.destination_if_false
            };

            items_thrown.push((destination, new_worry_level));
        }

        items_thrown
    }

    fn update_worry(&self, worry_level: u64) -> u64 {
        let left = match self.update_expression.0 {
            Operand::Int(value) => value,
            Operand::Old => worry_level,
        };
        let right = match self.update_expression.2 {
            Operand::Int(value) => value,
            Operand::Old => worry_level,
        };

        match self.update_expression.1 {
            Operator::Add => left + right,
            Operator::Mul => left * right,
        }
    }
}

struct KeepAway {
    monkeys: Vec<Monkey>,
    inspected_items: Vec<usize>,
    relief: u64,
    modulo_constant: u64,
}

impl KeepAway {
    fn new(monkeys: Vec<Monkey>, relief: u64) -> Self {
        let len = monkeys.len();
        let modulo_constant = monkeys
            .iter()
            .fold(1, |acc, monkey| acc * monkey.test_constant);
        Self {
            monkeys,
            inspected_items: vec![0; len],
            relief,
            modulo_constant,
        }
    }

    fn do_round(&mut self) {
        for i in 0..self.monkeys.len() {
            let thrown_items = self.monkeys[i].do_turn(self.relief);

            self.inspected_items[i] += thrown_items.len();

            for (j, item) in thrown_items {
                self.monkeys[j].items.push_back(item % self.modulo_constant);
            }
        }
    }
}

fn main() {
    let lines = aoc_2022::input_lines();
    let monkeys: Vec<Monkey> = lines
        .split(|line| line.is_empty())
        .map(|lines| Monkey::parse(lines))
        .collect();

    let mut game = KeepAway::new(monkeys.clone(), 3);
    for _ in 0..20 {
        game.do_round();
    }

    let monkey_business = {
        let mut inspections = game.inspected_items.clone();
        inspections.sort_by_key(|n| std::cmp::Reverse(*n));
        inspections[0] * inspections[1]
    };
    println!("01: {}", monkey_business);

    let mut game = KeepAway::new(monkeys, 1);
    for _ in 0..10000 {
        game.do_round();
    }

    let monkey_business = {
        let mut inspections = game.inspected_items.clone();
        inspections.sort_by_key(|n| std::cmp::Reverse(*n));
        inspections[0] * inspections[1]
    };
    println!("02: {}", monkey_business);
}

#[cfg(test)]
mod tests {
    use crate::{KeepAway, Monkey, Operand, Operator};
    use std::collections::VecDeque;

    #[test]
    fn examples_part01() {
        let monkeys = vec![
            Monkey {
                items: VecDeque::from([79, 98]),
                update_expression: (Operand::Old, Operator::Mul, Operand::Int(19)),
                test_constant: 23,
                destination_if_true: 2,
                destination_if_false: 3,
            },
            Monkey {
                items: VecDeque::from([54, 65, 75, 74]),
                update_expression: (Operand::Old, Operator::Add, Operand::Int(6)),
                test_constant: 19,
                destination_if_true: 2,
                destination_if_false: 0,
            },
            Monkey {
                items: VecDeque::from([79, 60, 97]),
                update_expression: (Operand::Old, Operator::Mul, Operand::Old),
                test_constant: 13,
                destination_if_true: 1,
                destination_if_false: 3,
            },
            Monkey {
                items: VecDeque::from([74]),
                update_expression: (Operand::Old, Operator::Add, Operand::Int(3)),
                test_constant: 17,
                destination_if_true: 0,
                destination_if_false: 1,
            },
        ];

        let mut game = KeepAway::new(monkeys, 3);

        for _ in 0..20 {
            game.do_round();
        }

        assert_eq!(vec![101, 95, 7, 105], game.inspected_items);
    }

    #[test]
    fn examples_part02() {
        let monkeys = vec![
            Monkey {
                items: VecDeque::from([79, 98]),
                update_expression: (Operand::Old, Operator::Mul, Operand::Int(19)),
                test_constant: 23,
                destination_if_true: 2,
                destination_if_false: 3,
            },
            Monkey {
                items: VecDeque::from([54, 65, 75, 74]),
                update_expression: (Operand::Old, Operator::Add, Operand::Int(6)),
                test_constant: 19,
                destination_if_true: 2,
                destination_if_false: 0,
            },
            Monkey {
                items: VecDeque::from([79, 60, 97]),
                update_expression: (Operand::Old, Operator::Mul, Operand::Old),
                test_constant: 13,
                destination_if_true: 1,
                destination_if_false: 3,
            },
            Monkey {
                items: VecDeque::from([74]),
                update_expression: (Operand::Old, Operator::Add, Operand::Int(3)),
                test_constant: 17,
                destination_if_true: 0,
                destination_if_false: 1,
            },
        ];

        let mut game = KeepAway::new(monkeys, 1);

        for _ in 0..10000 {
            game.do_round();
        }

        assert_eq!(vec![52166, 47830, 1938, 52013], game.inspected_items);
    }
}
