use std::{collections::HashSet, iter::FromIterator};

fn priority(ch: char) -> i32 {
    let offset = 1 + (ch.to_ascii_lowercase() as i32) - ('a' as i32);
    match ch {
        'a'..='z' => offset,
        'A'..='Z' => 26 + offset,
        _ => panic!("Invalid character: {}", ch),
    }
}

fn main() {
    let lines = aoc_2022::input_lines();

    let mut priorities_sum = 0;
    for line in &lines {
        assert!(line.len() % 2 == 0);

        let (one, two) = line.split_at(line.len() / 2);
        let set_one: HashSet<char> = HashSet::from_iter(one.chars());

        for ch in two.chars() {
            if set_one.contains(&ch) {
                priorities_sum += priority(ch);
                break;
            }
        }
    }
    println!("01: {}", priorities_sum);

    let mut badges_sum = 0;
    for chunk in lines.chunks(3) {
        let mut intersection: HashSet<char> = HashSet::from_iter(chunk[0].chars());
        for other in chunk.iter().skip(1) {
            let set: HashSet<char> = HashSet::from_iter(other.chars());
            intersection.retain(|e| set.contains(e));
        }
        badges_sum += priority(*intersection.iter().next().unwrap());
    }
    println!("02: {}", badges_sum);
}
