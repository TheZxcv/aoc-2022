fn from_snafu(number: &str) -> i64 {
    let mut value = 0;

    for (power, ch) in number.chars().rev().enumerate() {
        let power = power as u32;
        value += match ch {
            '2' => 2 * 5i64.pow(power),
            '1' => 5i64.pow(power),
            '0' => 0,
            '-' => -5i64.pow(power),
            '=' => -2 * 5i64.pow(power),
            _ => panic!("Invalid SNAFU digit."),
        };
    }

    value
}

fn to_snafu(value: i64) -> String {
    let mut number = String::new();
    let mut digits = 0;
    while 2 * 5i64.pow(digits) < value {
        digits += 1;
    }

    let mut target = value;
    for power in (0..=digits).rev() {
        let digit = ['2', '1', '0', '-', '=']
            .iter()
            .min_by_key(|ch| {
                (target
                    - match ch {
                        '2' => 2 * 5i64.pow(power),
                        '1' => 5i64.pow(power),
                        '0' => 0,
                        '-' => -5i64.pow(power),
                        '=' => -2 * 5i64.pow(power),
                        _ => panic!("Invalid SNAFU digit."),
                    })
                .abs()
            })
            .unwrap();

        number.push(*digit);
        target = target
            - match *digit {
                '2' => 2 * 5i64.pow(power),
                '1' => 5i64.pow(power),
                '0' => 0,
                '-' => -5i64.pow(power),
                '=' => -2 * 5i64.pow(power),
                _ => panic!("Invalid SNAFU digit."),
            }
    }

    assert_eq!(0, target);

    number
}

fn main() {
    let lines = aoc_2022::input_lines();
    let sum: i64 = lines.iter().map(|line| from_snafu(&line)).sum();
    println!("01: {}", to_snafu(sum));
}

#[cfg(test)]
mod tests {
    use crate::{from_snafu, to_snafu};

    #[test]
    fn from_snafu_tests() {
        assert_eq!(1, from_snafu("1"));
        assert_eq!(2, from_snafu("2"));
        assert_eq!(3, from_snafu("1="));
        assert_eq!(4, from_snafu("1-"));
        assert_eq!(5, from_snafu("10"));
        assert_eq!(6, from_snafu("11"));
        assert_eq!(7, from_snafu("12"));
        assert_eq!(8, from_snafu("2="));
        assert_eq!(9, from_snafu("2-"));
        assert_eq!(10, from_snafu("20"));
        assert_eq!(15, from_snafu("1=0"));
        assert_eq!(20, from_snafu("1-0"));
        assert_eq!(2022, from_snafu("1=11-2"));
        assert_eq!(12345, from_snafu("1-0---0"));
        assert_eq!(314159265, from_snafu("1121-1110-1=0"));
        assert_eq!(4800, from_snafu("2==200"));
    }

    #[test]
    fn to_snafu_tests() {
        assert_eq!("1", to_snafu(1));
        assert_eq!("2", to_snafu(2));
        assert_eq!("1=", to_snafu(3));
        assert_eq!("1-", to_snafu(4));
        assert_eq!("10", to_snafu(5));
        assert_eq!("11", to_snafu(6));
        assert_eq!("12", to_snafu(7));
        assert_eq!("2=", to_snafu(8));
        assert_eq!("2-", to_snafu(9));
        assert_eq!("20", to_snafu(10));
        assert_eq!("1=0", to_snafu(15));
        assert_eq!("1-0", to_snafu(20));
        assert_eq!("1=11-2", to_snafu(2022));
        assert_eq!("1-0---0", to_snafu(12345));
        assert_eq!("1121-1110-1=0", to_snafu(314159265));
        assert_eq!("2==200", to_snafu(4800));
    }
}
