#[derive(PartialEq, Eq)]
enum Tile {
    Void,
    Open,
    Wall,
}

impl Tile {
    fn parse(ch: char) -> Self {
        match ch {
            ' ' => Self::Void,
            '.' => Self::Open,
            '#' => Self::Wall,
            _ => panic!("Unknown tile type: {}.", ch),
        }
    }
}

#[derive(Copy, Clone)]
enum Direction {
    Right,
    Down,
    Left,
    Up,
}

impl Direction {
    fn from(dir: (isize, isize)) -> Self {
        match dir {
            (0, 1) => Self::Right,
            (1, 0) => Self::Down,
            (0, -1) => Self::Left,
            (-1, 0) => Self::Up,
            _ => panic!("Invalid direction: {:?}", dir),
        }
    }

    fn as_number(&self) -> usize {
        match self {
            Direction::Right => 0,
            Direction::Down => 1,
            Direction::Left => 2,
            Direction::Up => 3,
        }
    }

    fn to_vector(&self) -> (isize, isize) {
        match self {
            Direction::Right => (0, 1),
            Direction::Down => (1, 0),
            Direction::Left => (0, -1),
            Direction::Up => (-1, 0),
        }
    }
}

enum Action {
    Move(u32),
    TurnClockwise,
    TurnCounterClockwise,
}

impl Action {
    fn parse_many(line: &str) -> Vec<Self> {
        let mut actions = Vec::new();
        let mut chars = line.chars().peekable();
        while let Some(ch) = chars.next() {
            if ch == 'R' {
                actions.push(Self::TurnClockwise)
            } else if ch == 'L' {
                actions.push(Self::TurnCounterClockwise)
            } else if ch.is_digit(10) {
                let mut integer = String::with_capacity(2);
                integer.push(ch);
                while let Some(ch) = chars.peek() {
                    if !ch.is_digit(10) {
                        break;
                    }

                    integer.push(chars.next().unwrap());
                }

                actions.push(Self::Move(integer.parse().unwrap()));
            } else {
                panic!("Unrecognised action character: {}", ch)
            }
        }

        actions
    }
}

struct Grid {
    side: usize,
    faces: Vec<Vec<Vec<Tile>>>,
    positioning: Vec<Vec<i32>>,
}

impl Grid {
    fn parse(lines: &[String], side_len: usize) -> Self {
        let mut faces = Vec::new();
        let longest_line = lines.iter().max_by_key(|line| line.len()).unwrap().len();
        let faces_per_row = longest_line / side_len;
        let rows = lines.len() / side_len;

        assert!(lines.len() % side_len == 0);

        let mut positioning = vec![vec![-1; faces_per_row]; rows];
        let mut count = 0;

        for i in 0..rows {
            for j in 0..faces_per_row {
                let first = lines[i * side_len].chars().nth(j * side_len);
                if first.is_none() || first.unwrap() == ' ' {
                    continue;
                }

                positioning[i][j] = count;
                count += 1;

                let face: Vec<Vec<_>> = lines[(i * side_len)..((i + 1) * side_len)]
                    .iter()
                    .map(|s| {
                        s[(j * side_len)..((j + 1) * side_len)]
                            .chars()
                            .map(|ch| Tile::parse(ch))
                            .collect()
                    })
                    .collect();

                faces.push(face);
            }
        }

        Self {
            side: side_len,
            faces,
            positioning,
        }
    }

    fn follow_actions(
        &self,
        actions: &[Action],
        starting_pos: (usize, usize),
        is_cube: bool,
    ) -> ((usize, usize), Direction) {
        let mut position = (starting_pos.0 % self.side, starting_pos.1 % self.side, 0);
        let mut direction = (0, 1);

        for action in actions {
            let (new_position, new_direction) = self.apply(action, position, direction, is_cube);
            position = new_position;
            direction = new_direction;
        }

        let (y, x) = self.get_face_position(position.2);
        ((y + position.0, x + position.1), Direction::from(direction))
    }

    fn apply(
        &self,
        action: &Action,
        position: (usize, usize, i32),
        direction: (isize, isize),
        is_cube: bool,
    ) -> ((usize, usize, i32), (isize, isize)) {
        let mut position = position;
        let mut direction = direction;
        match action {
            Action::Move(amount) => {
                for _ in 0..*amount {
                    let (new_position, new_direction) = if !is_cube {
                        let pos = self.do_move(position, direction);
                        (pos, direction)
                    } else {
                        self.do_move_cube(position, direction)
                    };

                    if position == new_position {
                        break;
                    }
                    position = new_position;
                    direction = new_direction;
                }

                (position, direction)
            }
            Action::TurnClockwise => {
                let new_direction = match direction {
                    (0, 1) => (1, 0),
                    (1, 0) => (0, -1),
                    (0, -1) => (-1, 0),
                    (-1, 0) => (0, 1),
                    _ => panic!("Invalid direction: {:?}", direction),
                };

                (position, new_direction)
            }
            Action::TurnCounterClockwise => {
                let new_direction = match direction {
                    (0, 1) => (-1, 0),
                    (-1, 0) => (0, -1),
                    (0, -1) => (1, 0),
                    (1, 0) => (0, 1),
                    _ => panic!("Invalid direction: {:?}", direction),
                };

                (position, new_direction)
            }
        }
    }

    fn do_move(
        &self,
        position: (usize, usize, i32),
        direction: (isize, isize),
    ) -> (usize, usize, i32) {
        let face_id = position.2;
        assert!(face_id != -1);

        let absolute_position = {
            let (offy, offx) = self.get_face_position(face_id);
            (offy + position.0, offx + position.1)
        };

        let new_position = (
            Self::add_and_wrap(
                absolute_position.0,
                self.side * self.positioning.len(),
                direction.0,
            ),
            Self::add_and_wrap(
                absolute_position.1,
                self.side * self.positioning[0].len(),
                direction.1,
            ),
        );

        let (mut i, mut j, mut dest_id) = self.get_face_id(new_position);
        if face_id == dest_id {
            return if self.faces[face_id as usize][new_position.0 % self.side]
                [new_position.1 % self.side]
                == Tile::Open
            {
                (
                    new_position.0 % self.side,
                    new_position.1 % self.side,
                    face_id,
                )
            } else {
                position
            };
        }

        if dest_id == -1 {
            while self.positioning[i][j] == -1 {
                i = Self::add_and_wrap(i, self.positioning.len(), direction.0);
                j = Self::add_and_wrap(j, self.positioning[0].len(), direction.1);
            }

            dest_id = self.positioning[i][j];
        }

        let range = self.get_face_position(dest_id);
        let wrapped_position = match direction {
            (0, 1) => (new_position.0, range.1),
            (0, -1) => (new_position.0, range.1 + self.side - 1),
            (1, 0) => (range.0, new_position.1),
            (-1, 0) => (range.0 + self.side - 1, new_position.1),
            _ => panic!(),
        };

        if self.faces[dest_id as usize][wrapped_position.0 % self.side]
            [wrapped_position.1 % self.side]
            == Tile::Open
        {
            (
                wrapped_position.0 % self.side,
                wrapped_position.1 % self.side,
                dest_id,
            )
        } else {
            position
        }
    }

    fn get_face_position(&self, face_id: i32) -> (usize, usize) {
        for (i, row) in self.positioning.iter().enumerate() {
            for (j, id) in row.iter().enumerate() {
                if *id == face_id {
                    return (i * self.side, j * self.side);
                }
            }
        }

        panic!("Cannot find face {}", face_id);
    }

    fn get_face_id(&self, (y, x): (usize, usize)) -> (usize, usize, i32) {
        let i = (y / self.side) % self.positioning.len();
        let j = (x / self.side) % self.positioning[i].len();
        (i, j, self.positioning[i][j])
    }

    fn add_and_wrap(index: usize, length: usize, step: isize) -> usize {
        if index == 0 && step == -1 {
            length - 1
        } else if index == length - 1 && step == 1 {
            0
        } else {
            (index as isize + step) as usize
        }
    }

    fn do_move_cube(
        &self,
        position: (usize, usize, i32),
        direction: (isize, isize),
    ) -> ((usize, usize, i32), (isize, isize)) {
        let face_id = position.2;
        assert!(face_id != -1);

        let (dest_id, new_direction) = match (face_id, Direction::from(direction)) {
            (0, Direction::Right) => (1, Direction::Right),
            (0, Direction::Left) => (3, Direction::Right),
            (0, Direction::Down) => (2, Direction::Down),
            (0, Direction::Up) => (5, Direction::Right),

            (1, Direction::Right) => (4, Direction::Left),
            (1, Direction::Left) => (0, Direction::Left),
            (1, Direction::Down) => (2, Direction::Left),
            (1, Direction::Up) => (5, Direction::Up),

            (2, Direction::Right) => (1, Direction::Up),
            (2, Direction::Left) => (3, Direction::Down),
            (2, Direction::Down) => (4, Direction::Down),
            (2, Direction::Up) => (0, Direction::Up),

            (3, Direction::Right) => (4, Direction::Right),
            (3, Direction::Left) => (0, Direction::Right),
            (3, Direction::Down) => (5, Direction::Down),
            (3, Direction::Up) => (2, Direction::Right),

            (4, Direction::Right) => (1, Direction::Left),
            (4, Direction::Left) => (3, Direction::Left),
            (4, Direction::Down) => (5, Direction::Left),
            (4, Direction::Up) => (2, Direction::Up),

            (5, Direction::Right) => (4, Direction::Up),
            (5, Direction::Left) => (0, Direction::Down),
            (5, Direction::Down) => (1, Direction::Down),
            (5, Direction::Up) => (3, Direction::Up),
            _ => panic!(),
        };

        let new_x = position.0 as isize + direction.0;
        let new_y = position.1 as isize + direction.1;
        let (new_position, new_direction) = if new_x < 0
            || new_x >= self.side as isize
            || new_y < 0
            || new_y >= self.side as isize
        {
            let inner_position = match (Direction::from(direction), new_direction) {
                (Direction::Right, Direction::Right) => (position.0, 0),
                (Direction::Right, Direction::Left) => (self.side - position.0 - 1, self.side - 1),
                (Direction::Right, Direction::Down) => (0, self.side - position.0 - 1),
                (Direction::Right, Direction::Up) => (self.side - 1, position.0),

                (Direction::Left, Direction::Right) => (self.side - position.0 - 1, 0),
                (Direction::Left, Direction::Left) => (position.0, self.side - 1),
                (Direction::Left, Direction::Down) => (0, position.0),
                (Direction::Left, Direction::Up) => (self.side - 1, self.side - position.0 - 1),

                (Direction::Down, Direction::Right) => (self.side - position.0 - 1, 0),
                (Direction::Down, Direction::Left) => (position.1, self.side - 1),
                (Direction::Down, Direction::Down) => (0, position.1),
                (Direction::Down, Direction::Up) => (self.side - 1, self.side - position.1 - 1),

                (Direction::Up, Direction::Right) => (position.1, 0),
                (Direction::Up, Direction::Left) => (self.side - position.1 - 1, self.side - 1),
                (Direction::Up, Direction::Down) => (0, self.side - position.1 - 1),
                (Direction::Up, Direction::Up) => (self.side - 1, position.1),
            };

            (
                (inner_position.0, inner_position.1, dest_id),
                new_direction.to_vector(),
            )
        } else {
            ((new_x as usize, new_y as usize, position.2), direction)
        };

        if self.faces[new_position.2 as usize][new_position.0][new_position.1] == Tile::Open {
            (new_position, new_direction)
        } else {
            (position, direction)
        }
    }
}

fn main() {
    let lines = aoc_2022::input_lines();
    let separator = lines.iter().position(|e| e.is_empty()).unwrap();
    let grid = Grid::parse(&lines[0..separator], 50);
    let actions = Action::parse_many(&lines[separator + 1]);

    let (_, x) = grid.get_face_position(0);
    let starting_x = x;
    let (position, direction) = grid.follow_actions(&actions, (0, starting_x), false);

    let final_password = 1000 * (position.0 + 1) + 4 * (position.1 + 1) + direction.as_number();
    println!("01: {}", final_password);

    let (position, direction) = grid.follow_actions(&actions, (0, starting_x), true);
    let final_password = 1000 * (position.0 + 1) + 4 * (position.1 + 1) + direction.as_number();
    println!("02: {}", final_password);
}

#[cfg(test)]
mod tests {
    use crate::{Action, Grid};

    #[test]
    fn examples_part01() {
        let grid_lines = [
            "        ...#",
            "        .#..",
            "        #...",
            "        ....",
            "...#.......#",
            "........#...",
            "..#....#....",
            "..........#.",
            "        ...#....",
            "        .....#..",
            "        .#......",
            "        ......#.",
        ]
        .map(String::from);
        let grid = Grid::parse(&grid_lines, 4);
        let actions = Action::parse_many("10R5L5R10L4R5L5");

        let (_, starting_x) = grid.get_face_position(0);
        let (position, direction) = grid.follow_actions(&actions, (0, starting_x), false);

        assert_eq!(6, position.0 + 1);
        assert_eq!(8, position.1 + 1);
        assert_eq!(0, direction.as_number());
    }
}
