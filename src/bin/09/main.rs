use std::collections::HashSet;

#[derive(Clone, Copy)]
enum Action {
    Up,
    Down,
    Left,
    Right,
}

impl Action {
    fn parse_action(line: &str) -> (Self, i32) {
        let mut splits = line.split_ascii_whitespace();
        let action = splits.next().unwrap().chars().nth(0).unwrap();
        let distance: i32 = splits.next().unwrap().parse().unwrap();

        let action = match action {
            'U' => Self::Up,
            'D' => Self::Down,
            'L' => Self::Left,
            'R' => Self::Right,
            _ => panic!("Unrecognised action: {}", action),
        };

        (action, distance)
    }
}

struct Rope {
    head: (i32, i32),
    tails: Vec<(i32, i32)>,
}

impl Rope {
    fn apply_action(self, action: Action) -> Self {
        let new_head = match action {
            Action::Up => (self.head.0, self.head.1 + 1),
            Action::Down => (self.head.0, self.head.1 - 1),
            Action::Left => (self.head.0 - 1, self.head.1),
            Action::Right => (self.head.0 + 1, self.head.1),
        };

        Rope {
            head: new_head,
            tails: self
                .tails
                .iter()
                .scan(new_head, |head, tail| {
                    *head = Self::update_tail(head, *tail);
                    Some(*head)
                })
                .collect::<Vec<_>>(),
        }
    }

    fn update_tail((hx, hy): &(i32, i32), (tx, ty): (i32, i32)) -> (i32, i32) {
        if i32::abs(hx - tx) > 1 || i32::abs(hy - ty) > 1 {
            (tx + i32::signum(hx - tx), ty + i32::signum(hy - ty))
        } else {
            (tx, ty)
        }
    }
}

fn count_visited_by_tail(rope: Rope, actions: &[(Action, i32)]) -> u32 {
    let mut set: HashSet<(i32, i32)> = HashSet::new();
    let mut rope = rope;
    for (action, repeats) in actions {
        for _ in 0..*repeats {
            rope = rope.apply_action(*action);
            set.insert(*rope.tails.last().unwrap());
        }
    }

    set.len() as u32
}

fn main() {
    let lines = aoc_2022::input_lines();
    let actions = lines
        .iter()
        .map(|line| Action::parse_action(&line))
        .collect::<Vec<_>>();

    let rope = Rope {
        head: (0, 0),
        tails: vec![(0, 0); 1],
    };
    let count = count_visited_by_tail(rope, &actions);
    println!("01: {}", count);

    let rope = Rope {
        head: (0, 0),
        tails: vec![(0, 0); 9],
    };
    let count = count_visited_by_tail(rope, &actions);
    println!("02: {}", count);
}

#[cfg(test)]
mod tests {
    use crate::{count_visited_by_tail, Action, Rope};

    #[test]
    fn examples_part01() {
        let actions = [
            (Action::Right, 4),
            (Action::Up, 4),
            (Action::Left, 3),
            (Action::Down, 1),
            (Action::Right, 4),
            (Action::Down, 1),
            (Action::Left, 5),
            (Action::Right, 2),
        ];
        let rope = Rope {
            head: (0, 0),
            tails: vec![(0, 0); 1],
        };
        assert_eq!(13, count_visited_by_tail(rope, &actions));
    }

    #[test]
    fn examples_part02() {
        let actions = [
            (Action::Right, 5),
            (Action::Up, 8),
            (Action::Left, 8),
            (Action::Down, 3),
            (Action::Right, 17),
            (Action::Down, 10),
            (Action::Left, 25),
            (Action::Up, 20),
        ];
        let rope = Rope {
            head: (0, 0),
            tails: vec![(0, 0); 9],
        };
        assert_eq!(36, count_visited_by_tail(rope, &actions));
    }
}
