use std::collections::{HashMap, HashSet, VecDeque};

#[derive(Clone, Copy)]
struct Blizzard {
    initial: (usize, usize),
    dir: (isize, isize),
}

impl Blizzard {
    fn parse(p: (usize, usize), dir: char) -> Self {
        let direction = match dir {
            '>' => (1, 0),
            '<' => (-1, 0),
            '^' => (0, -1),
            'v' => (0, 1),
            _ => panic!("Invalid direction: {}", dir),
        };

        Self {
            initial: p,
            dir: direction,
        }
    }

    fn position_at_time(&self, minutes: i32, width: usize, height: usize) -> (usize, usize) {
        let time = if self.dir.0 != 0 {
            // Horizontal
            minutes as usize % width
        } else {
            // Vertical
            minutes as usize % (height - 2)
        };

        let mut pos = self.initial;
        for _ in 0..time {
            if self.dir.0 == -1 && pos.0 == 0 {
                pos.0 = width - 1;
            } else if self.dir.0 != 0 {
                pos.0 = ((pos.0 as isize + self.dir.0) as usize) % width;
            }

            if self.dir.1 == -1 && pos.1 == 1 {
                pos.1 = height - 2;
            } else if self.dir.1 == 1 && pos.1 == height - 2 {
                pos.1 = 1;
            } else if self.dir.1 != 0 {
                pos.1 = (pos.1 as isize + self.dir.1) as usize;
            }
        }

        pos
    }
}

struct Game {
    width: usize,
    height: usize,
    start: (usize, usize),
    end: (usize, usize),
    blizzards: Vec<Blizzard>,
}

impl Game {
    fn parse(lines: &[String]) -> Self {
        let width = lines[0].len() - 2;
        let height = lines.len();

        let sx = lines[0].chars().position(|ch| ch == '.').unwrap();
        let start = (sx - 1, 0);

        let ex = lines
            .last()
            .unwrap()
            .chars()
            .position(|ch| ch == '.')
            .unwrap();
        let end = (ex - 1, height - 1);

        let blizzards: Vec<_> = lines
            .iter()
            .enumerate()
            .flat_map(|(y, line)| {
                line.chars()
                    .enumerate()
                    .filter(|(_, ch)| *ch != '#' && *ch != '.')
                    .map(move |(x, ch)| Blizzard::parse((x - 1, y), ch))
            })
            .collect();

        Game {
            width,
            height,
            start,
            end,
            blizzards,
        }
    }

    fn shortest_path(&self, start: (usize, usize), end: (usize, usize), starting_time: i32) -> i32 {
        let mut cache: HashMap<i32, HashSet<(usize, usize)>> = HashMap::new();
        let mut visited: HashSet<((usize, usize), i32)> = HashSet::new();
        let mut states: VecDeque<((usize, usize), i32)> = VecDeque::new();

        states.push_back((start, starting_time));
        visited.insert((start, starting_time));

        while !states.is_empty() {
            let (pos, minutes) = states.pop_front().unwrap();
            let minutes = minutes + 1;

            if !cache.contains_key(&minutes) {
                let mut state = HashSet::new();
                for b in &self.blizzards {
                    state.insert(b.position_at_time(minutes, self.width, self.height));
                }

                cache.insert(minutes, state);
            }
            let current_state = cache.get(&minutes).unwrap();

            for (offx, offy) in [(0, 0), (0, 1), (0, -1), (-1, 0), (1, 0)] {
                let p = (pos.0 as isize + offx, pos.1 as isize + offy);

                if p.0 == end.0 as isize && p.1 == end.1 as isize {
                    return minutes;
                } else if (p.0 < 0
                    || p.0 >= self.width as isize
                    || p.1 < 1
                    || p.1 >= (self.height - 1) as isize)
                    && !(p.0 == start.0 as isize && p.1 == start.1 as isize)
                {
                    continue;
                }

                let next = (p.0 as usize, p.1 as usize);
                if visited.contains(&(next, minutes)) {
                    continue;
                }
                visited.insert((next, minutes));

                let is_occupied = current_state.contains(&next);
                if !is_occupied {
                    states.push_back((next, minutes));
                }
            }
        }

        panic!();
    }
}

fn main() {
    let lines = aoc_2022::input_lines();
    let game = Game::parse(&lines);

    let minutes = game.shortest_path(game.start, game.end, 0);
    println!("01: {}", minutes);

    let minutes = game.shortest_path(game.end, game.start, minutes);
    let minutes = game.shortest_path(game.start, game.end, minutes);
    println!("02: {}", minutes);
}

#[cfg(test)]
mod tests {
    use crate::Game;

    #[test]
    fn examples_part01() {
        let lines = [
            "#.######", "#>>.<^<#", "#.<..<<#", "#>v.><>#", "#<^v^^>#", "######.#",
        ]
        .map(String::from);
        let game = Game::parse(&lines);

        assert_eq!(18, game.shortest_path(game.start, game.end, 0));
    }

    #[test]
    fn examples_part02() {
        let lines = [
            "#.######", "#>>.<^<#", "#.<..<<#", "#>v.><>#", "#<^v^^>#", "######.#",
        ]
        .map(String::from);
        let game = Game::parse(&lines);

        let minutes = game.shortest_path(game.start, game.end, 0);
        assert_eq!(18, minutes);
        let minutes = game.shortest_path(game.end, game.start, minutes);
        assert_eq!(18 + 23, minutes);
        let minutes = game.shortest_path(game.start, game.end, minutes);
        assert_eq!(18 + 23 + 13, minutes);
    }
}
