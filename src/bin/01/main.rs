use std::cmp::Reverse;

fn main() {
    let lines = aoc_2022::input_lines();

    let calories = {
        let mut calories = lines
            .split(|e| e.is_empty())
            .into_iter()
            .map(|chunk| {
                chunk
                    .into_iter()
                    .filter_map(|e| e.parse::<i32>().ok())
                    .sum()
            })
            .collect::<Vec<i32>>();

        calories.sort_by_key(|x| Reverse(*x));
        calories
    };

    let max_calories = calories.first().unwrap();
    println!("01: {}", max_calories);

    let sum_top3 = calories[0] + calories[1] + calories[2];
    println!("02: {}", sum_top3);
}
