struct Map {
    map: Vec<Vec<u32>>,
    width: usize,
    height: usize,
}

impl Map {
    pub fn parse(lines: &[String]) -> Self {
        let map = lines
            .iter()
            .map(|line| {
                line.chars()
                    .map(|c| c.to_digit(10).unwrap())
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();

        let height = map.len();
        let width = map.first().unwrap().len();

        Self { map, width, height }
    }

    fn is_visible(&self, i: usize, j: usize) -> bool {
        i == 0
            || j == 0
            || i == self.height - 1
            || j == self.width - 1
            || self.map[i][j] > (0..i).map(|y| self.map[y][j]).max().unwrap()
            || self.map[i][j]
                > ((i + 1)..self.height)
                    .map(|y| self.map[y][j])
                    .max()
                    .unwrap()
            || self.map[i][j] > (0..j).map(|x| self.map[i][x]).max().unwrap()
            || self.map[i][j] > ((j + 1)..self.width).map(|x| self.map[i][x]).max().unwrap()
    }

    fn count_visible(&self) -> usize {
        let mut count = 0;
        for i in 0..self.height {
            for j in 0..self.width {
                if self.is_visible(i, j) {
                    count += 1;
                }
            }
        }

        count
    }

    fn get_scenic_score(&self, i: usize, j: usize) -> usize {
        let mut down = 0;
        for y in (i + 1)..self.height {
            down += 1;

            if self.map[i][j] <= self.map[y][j] {
                break;
            }
        }

        let mut up = 0;
        for y in (0..i).rev() {
            up += 1;

            if self.map[i][j] <= self.map[y][j] {
                break;
            }
        }

        let mut right = 0;
        for x in (j + 1)..self.width {
            right += 1;

            if self.map[i][j] <= self.map[i][x] {
                break;
            }
        }

        let mut left = 0;
        for x in (0..j).rev() {
            left += 1;

            if self.map[i][j] <= self.map[i][x] {
                break;
            }
        }

        up * down * left * right
    }

    fn max_scenic_score(&self) -> usize {
        let mut best = 0;
        for i in 0..self.height {
            for j in 0..self.width {
                best = usize::max(best, self.get_scenic_score(i, j))
            }
        }

        best
    }
}

fn main() {
    let lines = aoc_2022::input_lines();
    let map = Map::parse(&lines);

    println!("01: {}", map.count_visible());
    println!("02: {}", map.max_scenic_score());
}

#[cfg(test)]
mod tests {
    use crate::Map;

    #[test]
    fn examples_part01() {
        let lines = ["30373", "25512", "65332", "33549", "35390"].map(String::from);
        let map = Map::parse(&lines);
        assert_eq!(21, map.count_visible());
    }

    #[test]
    fn examples_part02() {
        let lines = ["30373", "25512", "65332", "33549", "35390"].map(String::from);
        let map = Map::parse(&lines);

        assert_eq!(8, map.max_scenic_score());
    }
}
