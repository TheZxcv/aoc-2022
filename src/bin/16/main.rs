use std::collections::{HashMap, VecDeque};

#[derive(Debug)]
struct Valve {
    name: String,
    flow_rate: i32,
    tunnels: Vec<String>,
}

impl Valve {
    fn parse(line: &str) -> Self {
        assert!(line.starts_with("Valve "));
        let space_idx = 6 + line[6..].find(' ').unwrap();
        let name = &line[6..space_idx];

        assert_eq!(" has flow rate=", &line[space_idx..(space_idx + 15)]);
        let semicolon_idx = space_idx + 15 + line[(space_idx + 15)..].find(';').unwrap();
        let flow_rate = line[(space_idx + 15)..semicolon_idx].parse().unwrap();

        assert_eq!("; tunnel", &line[semicolon_idx..(semicolon_idx + 8)]);
        let valve_idx = 5 + line.find("valve").unwrap();
        let space_idx = valve_idx + 1 + line[valve_idx..].find(' ').unwrap();
        let tunnels = line[space_idx..]
            .split(", ")
            .map(|s| s.to_string())
            .collect::<Vec<_>>();

        Self {
            name: name.to_string(),
            flow_rate,
            tunnels,
        }
    }
}

struct ValveNode {
    flow_rate: i32,
    tunnels: Vec<usize>,
}

struct ValveGraph {
    names_to_idx: HashMap<String, usize>,
    nodes: Vec<ValveNode>,
    distances_cache: Vec<Vec<i32>>,
}

impl ValveGraph {
    fn new(valves: &[Valve]) -> Self {
        let mut nodes = Vec::new();
        let mut names_to_idx: HashMap<String, usize> = HashMap::new();
        for valve in valves {
            names_to_idx.insert(valve.name.clone(), nodes.len());
            nodes.push(ValveNode {
                flow_rate: valve.flow_rate,
                tunnels: Vec::new(),
            });
        }

        for valve in valves {
            let idx = names_to_idx.get(&valve.name).unwrap();
            for tunnel in &valve.tunnels {
                let tunnel_idx = names_to_idx.get(tunnel).unwrap();
                nodes[*idx].tunnels.push(*tunnel_idx);
            }
        }

        Self {
            distances_cache: Self::build_cache(&nodes),
            nodes,
            names_to_idx,
        }
    }

    fn build_cache(nodes: &[ValveNode]) -> Vec<Vec<i32>> {
        (0..nodes.len())
            .map(|idx| Self::shortest_paths(nodes, idx))
            .collect::<Vec<Vec<i32>>>()
    }

    fn shortest_paths(nodes: &[ValveNode], start: usize) -> Vec<i32> {
        let mut distances = vec![-1; nodes.len()];
        let mut frontier = VecDeque::new();

        frontier.push_back((0, start));

        while let Some((distance, current)) = frontier.pop_front() {
            if distances[current] != -1 {
                continue;
            }

            distances[current] = distance;

            for &idx in &nodes[current].tunnels {
                if distances[idx] != -1 {
                    continue;
                }

                frontier.push_back((distance + 1, idx));
            }
        }

        distances
    }

    fn shortest_path(&self, start: usize, target: usize) -> i32 {
        self.distances_cache[start][target]
    }

    fn greatest_pressure_one(&self, start: usize, time: i32) -> i32 {
        let mut valve_open = vec![false; self.nodes.len()];
        self.greatest_pressure_one_rec(start, time, &mut valve_open)
    }

    fn greatest_pressure_one_rec(
        &self,
        start: usize,
        time: i32,
        valve_open: &mut Vec<bool>,
    ) -> i32 {
        if time <= 0 {
            return 0;
        }

        let mut best_score = 0;
        for i in 0..self.nodes.len() {
            let cost = self.shortest_path(start, i) + 1;
            if valve_open[i] || self.nodes[i].flow_rate == 0 || cost > time {
                continue;
            }

            valve_open[i] = true;
            let remaining_time = time - cost;
            let step_score = remaining_time * self.nodes[i].flow_rate;

            let sub_bestscore = self.greatest_pressure_one_rec(i, remaining_time, valve_open);
            best_score = best_score.max(step_score + sub_bestscore);
            valve_open[i] = false;
        }

        best_score
    }

    fn greatest_pressure_two(&self, start: usize, time: i32) -> i32 {
        let mut valve_open = vec![false; self.nodes.len()];
        self.greatest_pressure_two_rec(start, start, time, time, &mut valve_open)
    }

    fn greatest_pressure_two_rec(
        &self,
        start1: usize,
        start2: usize,
        time1: i32,
        time2: i32,
        valve_open: &mut Vec<bool>,
    ) -> i32 {
        if time1 <= 0 && time2 <= 0 {
            return 0;
        }

        if time2 == 0 {
            return self.greatest_pressure_one_rec(start1, time1, valve_open);
        } else if time1 == 0 {
            return self.greatest_pressure_one_rec(start2, time2, valve_open);
        }

        let mut best_score = 0;
        let mut no_move_for1 = true;
        let mut no_move_for2 = true;

        for i in 0..self.nodes.len() {
            let cost1 = self.shortest_path(start1, i) + 1;
            if valve_open[i] || self.nodes[i].flow_rate == 0 || cost1 > time1 {
                continue;
            }
            no_move_for1 = false;

            for j in 0..self.nodes.len() {
                let cost2 = self.shortest_path(start2, j) + 1;
                if i == j || valve_open[j] || self.nodes[j].flow_rate == 0 || cost2 > time2 {
                    continue;
                }

                no_move_for2 = false;

                valve_open[i] = true;
                valve_open[j] = true;

                let remaining_time1 = time1 - cost1;
                let remaining_time2 = time2 - cost2;
                let step_score = remaining_time1 * self.nodes[i].flow_rate
                    + remaining_time2 * self.nodes[j].flow_rate;

                let sub_bestscore = self.greatest_pressure_two_rec(
                    i,
                    j,
                    remaining_time1,
                    remaining_time2,
                    valve_open,
                );
                best_score = i32::max(best_score, step_score + sub_bestscore);

                valve_open[i] = false;
                valve_open[j] = false;
            }
        }

        if no_move_for1 {
            self.greatest_pressure_one_rec(start2, time2, valve_open)
        } else if no_move_for2 {
            self.greatest_pressure_one_rec(start1, time1, valve_open)
        } else {
            best_score
        }
    }
}

fn main() {
    let lines = aoc_2022::input_lines();
    let valves = lines
        .iter()
        .map(|line| Valve::parse(&line))
        .collect::<Vec<_>>();

    let graph = ValveGraph::new(&valves);

    let start_position = *graph.names_to_idx.get("AA").unwrap();
    let pressure = graph.greatest_pressure_one(start_position, 30);
    println!("01: {}", pressure);

    let pressure = graph.greatest_pressure_two(start_position, 26);
    println!("02: {}", pressure);
}

#[cfg(test)]
mod tests {
    use crate::{Valve, ValveGraph};

    #[test]
    fn examples_part01() {
        let lines = [
            "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB",
            "Valve BB has flow rate=13; tunnels lead to valves CC, AA",
            "Valve CC has flow rate=2; tunnels lead to valves DD, BB",
            "Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE",
            "Valve EE has flow rate=3; tunnels lead to valves FF, DD",
            "Valve FF has flow rate=0; tunnels lead to valves EE, GG",
            "Valve GG has flow rate=0; tunnels lead to valves FF, HH",
            "Valve HH has flow rate=22; tunnel leads to valve GG",
            "Valve II has flow rate=0; tunnels lead to valves AA, JJ",
            "Valve JJ has flow rate=21; tunnel leads to valve II",
        ]
        .map(String::from);
        let valves = lines
            .iter()
            .map(|line| Valve::parse(&line))
            .collect::<Vec<_>>();

        let graph = ValveGraph::new(&valves);

        let start_position = *graph.names_to_idx.get("AA").unwrap();
        let pressure = graph.greatest_pressure_one(start_position, 30);
        assert_eq!(1651, pressure);
    }

    #[test]
    fn examples_part02() {
        let lines = [
            "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB",
            "Valve BB has flow rate=13; tunnels lead to valves CC, AA",
            "Valve CC has flow rate=2; tunnels lead to valves DD, BB",
            "Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE",
            "Valve EE has flow rate=3; tunnels lead to valves FF, DD",
            "Valve FF has flow rate=0; tunnels lead to valves EE, GG",
            "Valve GG has flow rate=0; tunnels lead to valves FF, HH",
            "Valve HH has flow rate=22; tunnel leads to valve GG",
            "Valve II has flow rate=0; tunnels lead to valves AA, JJ",
            "Valve JJ has flow rate=21; tunnel leads to valve II",
        ]
        .map(String::from);
        let valves = lines
            .iter()
            .map(|line| Valve::parse(&line))
            .collect::<Vec<_>>();

        let graph = ValveGraph::new(&valves);

        let start_position = *graph.names_to_idx.get("AA").unwrap();
        let pressure = graph.greatest_pressure_two(start_position, 26);
        assert_eq!(1707, pressure);
    }
}
