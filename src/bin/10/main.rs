enum Instruction {
    Noop,
    AddX(i32),
}

impl Instruction {
    fn parse(line: &str) -> Self {
        let mut splits = line.split_ascii_whitespace();
        let mnemonic = splits.next().unwrap();
        match mnemonic {
            "noop" => Self::Noop,
            "addx" => Self::AddX(splits.next().unwrap().parse().unwrap()),
            _ => panic!("Unknown instuction: {}", mnemonic),
        }
    }

    fn apply(&self, x: i32) -> i32 {
        match self {
            Self::Noop => x,
            Self::AddX(v) => x + v,
        }
    }

    fn cycles(&self) -> u32 {
        match self {
            Self::Noop => 1,
            Self::AddX(_) => 2,
        }
    }
}

struct CycleValues<'a> {
    x: i32,
    current_idx: usize,
    cycles: u32,
    instructions: &'a [Instruction],
}

impl Iterator for CycleValues<'_> {
    type Item = i32;

    fn next(&mut self) -> Option<Self::Item> {
        if self.cycles == 0 {
            if self.current_idx >= self.instructions.len() {
                return None;
            }

            self.x = self.instructions[self.current_idx].apply(self.x);

            self.current_idx += 1;
            if self.current_idx < self.instructions.len() {
                self.cycles = self.instructions[self.current_idx].cycles();
            }
        }

        if self.cycles > 0 {
            self.cycles -= 1;
        }

        Some(self.x)
    }
}

impl CycleValues<'_> {
    fn new(instructions: &[Instruction]) -> CycleValues {
        CycleValues {
            x: 1,
            current_idx: 0,
            cycles: if let Some(el) = instructions.first() {
                el.cycles()
            } else {
                0
            },
            instructions,
        }
    }
}

fn calculate_signal_strength(instructions: &[Instruction]) -> i32 {
    let values = CycleValues::new(&instructions);
    values
        .take(220)
        .enumerate()
        .filter(|(i, _)| match i + 1 {
            20 => true,
            60 => true,
            100 => true,
            140 => true,
            180 => true,
            220 => true,
            _ => false,
        })
        .map(|(i, x)| (i + 1) as i32 * x)
        .sum()
}

fn main() {
    let lines = aoc_2022::input_lines();
    let instructions: Vec<_> = lines.iter().map(|line| Instruction::parse(&line)).collect();

    let signal_strength: i32 = calculate_signal_strength(&instructions);
    println!("01: {}", signal_strength);

    let mut screen = [false; 40 * 6];
    let values = CycleValues::new(&instructions);
    for (cycle, x) in values.enumerate() {
        let horizontal = (cycle % 40) as i32;
        screen[cycle % 240] = horizontal == x - 1 || horizontal == x || horizontal == x + 1;
    }

    println!("02:");
    for i in 0..6 {
        for j in 0..40 {
            if screen[i * 40 + j] {
                print!("#");
            } else {
                print!(".");
            }
        }

        println!();
    }
}

#[cfg(test)]
mod tests {
    use crate::{CycleValues, Instruction};

    #[test]
    fn examples_part01() {
        let instructions = [
            Instruction::Noop,
            Instruction::AddX(3),
            Instruction::AddX(-5),
        ];

        let mut cycles = CycleValues::new(&instructions);

        assert_eq!(1, cycles.next().unwrap());
        assert_eq!(1, cycles.next().unwrap());
        assert_eq!(1, cycles.next().unwrap());
        assert_eq!(4, cycles.next().unwrap());
        assert_eq!(4, cycles.next().unwrap());
        assert_eq!(-1, cycles.next().unwrap());
        assert!(cycles.next().is_none());
    }
}
