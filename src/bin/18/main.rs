use std::collections::{HashSet, VecDeque};

type Cube = (i32, i32, i32);

#[derive(Eq, PartialEq, Hash)]
enum Face {
    Top,
    Bottom,
    Left,
    Right,
    Front,
    Back,
}

fn parse_cube(line: &str) -> Cube {
    let mut splits = line.split(',');
    let x = splits.next().unwrap().parse().unwrap();
    let y = splits.next().unwrap().parse().unwrap();
    let z = splits.next().unwrap().parse().unwrap();
    (x, y, z)
}

fn count_uncovered_faces(cubes: &HashSet<Cube>) -> i32 {
    let mut faces = 0;
    for (x, y, z) in cubes {
        let mut covered = 0;
        for (off_x, off_y, off_z) in [
            (1, 0, 0),
            (0, 1, 0),
            (0, 0, 1),
            (-1, 0, 0),
            (0, -1, 0),
            (0, 0, -1),
        ] {
            if cubes.contains(&(x + off_x, y + off_y, z + off_z)) {
                covered += 1;
            }
        }

        assert!(covered <= 6);

        faces += 6 - covered;
    }

    faces
}

fn calculate_exterior_surface(cubes: &HashSet<Cube>) -> i32 {
    let (min_x, min_y, min_z) = {
        (
            cubes.iter().map(|c| c.0).min().unwrap() - 1,
            cubes.iter().map(|c| c.1).min().unwrap() - 1,
            cubes.iter().map(|c| c.2).min().unwrap() - 1,
        )
    };
    let (max_x, max_y, max_z) = {
        (
            cubes.iter().map(|c| c.0).max().unwrap() + 1,
            cubes.iter().map(|c| c.1).max().unwrap() + 1,
            cubes.iter().map(|c| c.2).max().unwrap() + 1,
        )
    };

    let mut touched: HashSet<(Face, Cube)> = HashSet::new();
    let mut visited: HashSet<Cube> = HashSet::new();
    let mut frontier = VecDeque::new();

    frontier.push_back((0, 0, 0));

    while let Some(position) = frontier.pop_front() {
        if visited.contains(&position) {
            continue;
        }

        visited.insert(position);

        let (x, y, z) = position;
        for (face, (off_x, off_y, off_z)) in [
            (Face::Left, (1, 0, 0)),
            (Face::Bottom, (0, 1, 0)),
            (Face::Back, (0, 0, 1)),
            (Face::Right, (-1, 0, 0)),
            (Face::Top, (0, -1, 0)),
            (Face::Front, (0, 0, -1)),
        ] {
            let neighbour = (x + off_x, y + off_y, z + off_z);
            if visited.contains(&neighbour)
                || neighbour.0 < min_x
                || neighbour.0 > max_x
                || neighbour.1 < min_y
                || neighbour.1 > max_y
                || neighbour.2 < min_z
                || neighbour.2 > max_z
            {
                continue;
            }

            if !cubes.contains(&neighbour) {
                frontier.push_back(neighbour);
            } else {
                touched.insert((face, neighbour));
            }
        }
    }

    touched.len() as i32
}

fn main() {
    let lines = aoc_2022::input_lines();
    let cubes = lines
        .iter()
        .map(|line| parse_cube(&line))
        .collect::<HashSet<_>>();

    println!("01: {}", count_uncovered_faces(&cubes));
    println!("02: {}", calculate_exterior_surface(&cubes));
}

#[cfg(test)]
mod tests {
    use crate::{calculate_exterior_surface, count_uncovered_faces};
    use std::collections::HashSet;

    #[test]
    fn examples_part01() {
        let cubes = [
            (2, 2, 2),
            (1, 2, 2),
            (3, 2, 2),
            (2, 1, 2),
            (2, 3, 2),
            (2, 2, 1),
            (2, 2, 3),
            (2, 2, 4),
            (2, 2, 6),
            (1, 2, 5),
            (3, 2, 5),
            (2, 1, 5),
            (2, 3, 5),
        ]
        .iter()
        .cloned()
        .collect::<HashSet<_>>();
        assert_eq!(64, count_uncovered_faces(&cubes));
    }

    #[test]
    fn examples_part02() {
        let cubes = [
            (2, 2, 2),
            (1, 2, 2),
            (3, 2, 2),
            (2, 1, 2),
            (2, 3, 2),
            (2, 2, 1),
            (2, 2, 3),
            (2, 2, 4),
            (2, 2, 6),
            (1, 2, 5),
            (3, 2, 5),
            (2, 1, 5),
            (2, 3, 5),
        ]
        .iter()
        .cloned()
        .collect::<HashSet<_>>();
        assert_eq!(58, calculate_exterior_surface(&cubes));
    }
}
