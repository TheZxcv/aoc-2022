use std::convert::TryInto;

struct Stroke {
    start: (usize, usize),
    end: (usize, usize),
    step: (isize, isize),
    current: Option<(usize, usize)>,
}

impl Stroke {
    fn new(start: (usize, usize), end: (usize, usize)) -> Self {
        Stroke {
            start,
            end,
            step: (
                isize::signum((end.0 as isize) - (start.0 as isize)),
                isize::signum((end.1 as isize) - (start.1 as isize)),
            ),
            current: None,
        }
    }
}

impl Iterator for Stroke {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        self.current = if self.current.is_none() {
            Some(self.start)
        } else {
            let current = self.current.unwrap();

            if current == self.end {
                None
            } else {
                Some((
                    (current.0 as isize + self.step.0).try_into().unwrap(),
                    (current.1 as isize + self.step.1).try_into().unwrap(),
                ))
            }
        };

        return self.current;
    }
}

fn parse_path(line: &str) -> impl Iterator<Item = (usize, usize)> {
    let points: Vec<(usize, usize)> = line
        .split(" -> ")
        .map(|point| {
            let comma = point.find(',').unwrap();
            (
                point[..comma].parse().unwrap(),
                point[comma + 1..].parse().unwrap(),
            )
        })
        .collect();

    (1..points.len())
        .map(move |i| Stroke::new(points[i - 1].clone(), points[i].clone()))
        .flatten()
}

fn add_grain(grid: &mut Vec<Vec<bool>>, source: (usize, usize)) -> bool {
    if grid[source.0][source.1] {
        return false;
    }

    let height = grid[0].len();
    let width = grid.len();

    let mut land_x = source.0;
    let mut land_y = source.1;

    loop {
        land_y = {
            let start_y = land_y;
            let mut land_y = start_y;
            for y in start_y..=height {
                if y == height {
                    land_y = height;
                    break;
                } else if grid[land_x][y] {
                    break;
                } else {
                    land_y = y
                }
            }

            land_y
        };

        if land_y >= height || land_x == 0 || land_x == width - 1 {
            return false;
        }

        if !grid[land_x - 1][land_y + 1] {
            land_x -= 1;
            land_y += 1;
        } else if !grid[land_x + 1][land_y + 1] {
            land_x += 1;
            land_y += 1;
        }

        if land_y < height - 1
            && grid[land_x][land_y + 1]
            && (land_x > 0 && grid[land_x - 1][land_y + 1])
            && (land_x < width - 1 && grid[land_x + 1][land_y + 1])
        {
            break;
        }
    }

    grid[land_x][land_y] = true;

    true
}

fn main() {
    let lines = aoc_2022::input_lines();
    let points = lines
        .iter()
        .flat_map(|line| parse_path(line))
        .collect::<Vec<_>>();

    let offset_x = 250;
    let (max_x, max_y) = points.iter().fold((0, 0), |acc, el| {
        (usize::max(acc.0, el.0), usize::max(acc.1, el.1))
    });

    let mut grid = vec![vec![false; max_y + 3]; max_x + 1];
    for &(x, y) in &points {
        grid[x - offset_x][y] = true;
    }

    let mut grains = 0;
    while add_grain(&mut grid, (500 - offset_x, 0)) {
        grains += 1;
    }
    println!("01: {}", grains);

    for x in 0..grid.len() {
        *grid[x].last_mut().unwrap() = true;
    }

    while add_grain(&mut grid, (500 - offset_x, 0)) {
        grains += 1;
    }
    println!("02: {}", grains);
}
