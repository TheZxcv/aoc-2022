fn apply(
    meta_index: usize,
    encrypted: &mut Vec<i64>,
    indices: &mut Vec<usize>,
    reverse: &mut Vec<usize>,
) {
    let idx = indices[meta_index];
    let steps = encrypted[idx] as isize % (encrypted.len() as isize - 1);

    if steps == 0 {
        return;
    } else if steps > 0 {
        let mut steps = steps;
        let mut idx = idx;
        while steps > 0 {
            let other_idx = if idx == encrypted.len() - 1 {
                0
            } else {
                idx + 1
            };

            encrypted.swap(idx, other_idx);
            let other_meta_index = reverse[other_idx];
            indices.swap(meta_index, other_meta_index);
            reverse.swap(idx, other_idx);

            idx = other_idx;
            steps -= 1;
        }
    } else {
        let mut steps = steps;
        let mut idx = idx;
        while steps < 0 {
            let other_idx = if idx == 0 {
                encrypted.len() - 1
            } else {
                idx - 1
            };

            encrypted.swap(idx, other_idx);
            let other_meta_index = reverse[other_idx];
            indices.swap(meta_index, other_meta_index);
            reverse.swap(idx, other_idx);

            idx = other_idx;
            steps += 1;
        }
    }
}

fn mixing(
    encrypted: Vec<i64>,
    indices: Vec<usize>,
    reverse: Vec<usize>,
) -> (Vec<i64>, Vec<usize>, Vec<usize>) {
    let mut encrypted = encrypted;
    let mut indices = indices;
    let mut reverse_indices = reverse;

    for i in 0..encrypted.len() {
        apply(i, &mut encrypted, &mut indices, &mut reverse_indices);
    }

    (encrypted, indices, reverse_indices)
}

fn get_final_values(encrypted: Vec<i64>) -> (i64, i64, i64) {
    let zero_idx = encrypted.iter().position(|&v| v == 0).unwrap();

    (
        encrypted[(zero_idx + 1000) % encrypted.len()],
        encrypted[(zero_idx + 2000) % encrypted.len()],
        encrypted[(zero_idx + 3000) % encrypted.len()],
    )
}

fn decrypt(encrypted: &[i64], rounds: i32) -> (i64, i64, i64) {
    let mut encrypted: Vec<_> = encrypted.to_vec();
    let mut indices: Vec<_> = (0..encrypted.len()).collect();
    let mut reverse: Vec<_> = (0..encrypted.len()).collect();
    for _ in 0..rounds {
        let (v, i, r) = mixing(encrypted, indices, reverse);
        encrypted = v;
        indices = i;
        reverse = r;
    }

    get_final_values(encrypted)
}

fn main() {
    let lines = aoc_2022::input_lines();
    let values: Vec<i64> = lines.iter().map(|line| line.parse().unwrap()).collect();

    let (x, y, z) = decrypt(&values, 1);
    println!("01: {}", x + y + z);

    let values: Vec<_> = values.into_iter().map(|v| v * 811589153).collect();
    let (x, y, z) = decrypt(&values, 10);
    println!("02: {}", x + y + z);
}

#[cfg(test)]
mod tests {
    use crate::decrypt;

    #[test]
    fn examples_part01() {
        let values = [1, 2, -3, 3, -2, 0, 4];
        let (x, y, z) = decrypt(&values, 1);
        assert_eq!(4, x);
        assert_eq!(-3, y);
        assert_eq!(2, z);
    }

    #[test]
    fn examples_part02() {
        let values = [1, 2, -3, 3, -2, 0, 4].map(|v| v * 811589153);
        let (x, y, z) = decrypt(&values, 10);
        assert_eq!(811589153, x);
        assert_eq!(2434767459, y);
        assert_eq!(-1623178306, z);
    }
}
