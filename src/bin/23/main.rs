use std::collections::HashMap;

#[derive(Eq, PartialEq, Copy, Clone)]
enum Direction {
    North,
    South,
    West,
    East,
}

#[derive(Copy, Clone)]
struct Elf {
    position: (i32, i32),
    order: [Direction; 4],
}

impl Elf {
    fn new(position: (i32, i32)) -> Self {
        Self {
            position,
            order: [
                Direction::North,
                Direction::South,
                Direction::West,
                Direction::East,
            ],
        }
    }

    fn move_to_last(&mut self) {
        self.update(self.order[0]);
    }

    fn update(&mut self, dir: Direction) {
        let idx = self.order.iter().position(|d| *d == dir).unwrap();
        for i in idx..(self.order.len() - 1) {
            self.order.swap(i, i + 1);
        }
    }

    fn change_position(&self, p: (i32, i32)) -> Self {
        Self {
            position: p,
            order: self.order,
        }
    }
}

fn move_elf(elf: &Elf, elves: &HashMap<(i32, i32), Elf>) -> Option<(i32, i32)> {
    let mut has_neighbour = false;
    for (offx, offy) in [
        (0, -1),
        (0, 1),
        (-1, 0),
        (1, 0),
        (-1, -1),
        (1, 1),
        (1, -1),
        (-1, 1),
    ] {
        if elves.contains_key(&(elf.position.0 + offx, elf.position.1 + offy)) {
            has_neighbour = true;
            break;
        }
    }

    if !has_neighbour {
        return None;
    }

    for dir in elf.order {
        let (diag1, pos, diag2) = match dir {
            Direction::North => (
                (elf.position.0 - 1, elf.position.1 - 1),
                (elf.position.0, elf.position.1 - 1),
                (elf.position.0 + 1, elf.position.1 - 1),
            ),
            Direction::South => (
                (elf.position.0 - 1, elf.position.1 + 1),
                (elf.position.0, elf.position.1 + 1),
                (elf.position.0 + 1, elf.position.1 + 1),
            ),
            Direction::West => (
                (elf.position.0 - 1, elf.position.1 - 1),
                (elf.position.0 - 1, elf.position.1),
                (elf.position.0 - 1, elf.position.1 + 1),
            ),
            Direction::East => (
                (elf.position.0 + 1, elf.position.1 - 1),
                (elf.position.0 + 1, elf.position.1),
                (elf.position.0 + 1, elf.position.1 + 1),
            ),
        };

        if !elves.contains_key(&diag1) && !elves.contains_key(&pos) && !elves.contains_key(&diag2) {
            return Some(pos);
        }
    }

    None
}

fn move_all_elves(elves: HashMap<(i32, i32), Elf>) -> (bool, HashMap<(i32, i32), Elf>) {
    let mut chosen: HashMap<(i32, i32), u32> = HashMap::new();
    let mut moves = Vec::new();
    for (_, elf) in &elves {
        let result = move_elf(&elf, &elves);
        moves.push((elf, result));

        if let Some(new_position) = result {
            if let Some(cnt) = chosen.get_mut(&new_position) {
                *cnt += 1;
            } else {
                chosen.insert(new_position, 1);
            }
        }
    }

    let mut no_update = true;
    let mut moved_elves: HashMap<(i32, i32), Elf> = HashMap::new();
    for (elf, m) in moves {
        let mut moved_elf = if let Some(new_position) = m {
            if let Some(1) = chosen.get(&new_position) {
                no_update = false;
                elf.change_position(new_position)
            } else {
                *elf
            }
        } else {
            *elf
        };

        moved_elf.move_to_last();

        moved_elves.insert(moved_elf.position, moved_elf);
    }

    (no_update, moved_elves)
}

fn main() {
    let lines = aoc_2022::input_lines();
    let elves: Vec<Elf> = lines
        .iter()
        .enumerate()
        .flat_map(|(i, line)| {
            line.chars()
                .enumerate()
                .filter(|(_, ch)| *ch == '#')
                .map(move |(j, _)| Elf::new((j as i32, i as i32)))
        })
        .collect();
    let elves: HashMap<(i32, i32), Elf> = {
        let mut map = HashMap::new();
        for elf in elves {
            map.insert(elf.position, elf);
        }

        map
    };

    let mut elves = elves;
    for _ in 0..10 {
        let (_, e) = move_all_elves(elves);
        elves = e;
    }

    let top_y = elves.keys().map(|k| k.1).min().unwrap();
    let bottom_y = elves.keys().map(|k| k.1).max().unwrap();
    let max_x = elves.keys().map(|k| k.0).max().unwrap();
    let min_x = elves.keys().map(|k| k.0).min().unwrap();

    let count = (max_x - min_x + 1) * (bottom_y - top_y + 1) - elves.len() as i32;
    println!("01: {}", count);

    let mut rounds = 10;
    loop {
        rounds += 1;
        let (no_update, e) = move_all_elves(elves);
        elves = e;

        if no_update {
            break;
        }
    }
    println!("02: {}", rounds);
}
