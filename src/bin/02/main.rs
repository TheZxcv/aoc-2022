#[derive(PartialEq, Copy, Clone)]
pub enum Hand {
    Rock,
    Paper,
    Scissors,
}

impl Hand {
    fn parse(s: &str) -> Self {
        match s.chars().nth(0).unwrap() {
            'A' => Self::Rock,
            'B' => Self::Paper,
            'C' => Self::Scissors,
            'X' => Self::Rock,
            'Y' => Self::Paper,
            'Z' => Self::Scissors,
            _ => panic!(),
        }
    }

    fn value(&self) -> i32 {
        match self {
            Self::Rock => 1,
            Self::Paper => 2,
            Self::Scissors => 3,
        }
    }
}

pub enum Action {
    Lose,
    Draw,
    Win,
}

impl Action {
    fn parse(s: &str) -> Self {
        match s.chars().nth(0).unwrap() {
            'X' => Self::Lose,
            'Y' => Self::Draw,
            'Z' => Self::Win,
            _ => panic!(),
        }
    }

    fn value(&self) -> i32 {
        match self {
            Self::Lose => 0,
            Self::Draw => 3,
            Self::Win => 6,
        }
    }
}

fn calculate_points_strategy1(opponent: Hand, player: Hand) -> i32 {
    let outcome = if opponent == player {
        3
    } else if player == Hand::Rock && opponent == Hand::Scissors
        || player == Hand::Paper && opponent == Hand::Rock
        || player == Hand::Scissors && opponent == Hand::Paper
    {
        6
    } else {
        0
    };

    player.value() + outcome
}

fn calculate_points_strategy2(opponent: Hand, action: Action) -> i32 {
    let hand_value = match action {
        Action::Win => match opponent {
            Hand::Rock => Hand::Paper.value(),
            Hand::Paper => Hand::Scissors.value(),
            Hand::Scissors => Hand::Rock.value(),
        },
        Action::Lose => match opponent {
            Hand::Rock => Hand::Scissors.value(),
            Hand::Paper => Hand::Rock.value(),
            Hand::Scissors => Hand::Paper.value(),
        },
        Action::Draw => opponent.value(),
    };

    return action.value() + hand_value;
}

fn main() {
    let lines = aoc_2022::input_lines();

    let mut total1 = 0;
    let mut total2 = 0;
    for line in lines {
        if line.is_empty() {
            continue;
        }

        let mut splits = line.splitn(2, ' ');
        let a = Hand::parse(splits.next().unwrap());
        let symbol = splits.next().unwrap();

        let b = Hand::parse(symbol);
        total1 += calculate_points_strategy1(a, b);

        let action = Action::parse(symbol);
        total2 += calculate_points_strategy2(a, action);
    }

    println!("01: {}", total1);
    println!("02: {}", total2);
}
