use std::str::Chars;

#[derive(Eq)]
enum Message {
    Int(i32),
    List(Vec<Message>),
}

impl Message {
    fn parse(line: &str) -> Self {
        let mut chars = line.chars().peekable();
        Self::parse_list(&mut chars)
    }

    fn parse_list(chars: &mut std::iter::Peekable<Chars>) -> Self {
        assert_eq!(Some('['), chars.next());

        let mut content = Vec::new();
        while chars.peek().is_some() && *chars.peek().unwrap() != ']' {
            if *chars.peek().unwrap() == ',' {
                chars.next();
            }

            content.push(Self::parse_element(chars));
        }

        assert_eq!(Some(']'), chars.next());

        Self::List(content)
    }

    fn parse_element(chars: &mut std::iter::Peekable<Chars>) -> Self {
        if let Some('[') = chars.peek() {
            Self::parse_list(chars)
        } else {
            let mut integer = String::with_capacity(2);
            while let Some(ch) = chars.peek() {
                if !ch.is_digit(10) {
                    break;
                }

                integer.push(chars.next().unwrap());
            }

            Self::Int(integer.parse().unwrap())
        }
    }
}

impl Ord for Message {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match (self, other) {
            (Self::Int(a), Self::Int(b)) => a.cmp(b),
            (Self::List(a), Self::List(b)) => {
                for (x, y) in a.iter().zip(b.iter()) {
                    let cmp = x.cmp(y);
                    if cmp.is_ne() {
                        return cmp;
                    }
                }

                a.len().cmp(&b.len())
            }
            (Self::Int(a), Self::List(_)) => Self::List(vec![Self::Int(*a)]).cmp(other),
            (Self::List(_), Self::Int(b)) => self.cmp(&Self::List(vec![Self::Int(*b)])),
        }
    }
}

impl PartialOrd for Message {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Message {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Int(a), Self::Int(b)) => a.eq(b),
            (Self::List(a), Self::List(b)) => a.eq(b),
            _ => false,
        }
    }
}

fn main() {
    let lines = aoc_2022::input_lines();

    let pairs = lines
        .split(|line| line.is_empty())
        .map(|line| (Message::parse(&line[0]), Message::parse(&line[1])))
        .collect::<Vec<_>>();

    let count: usize = pairs
        .iter()
        .enumerate()
        .filter(|(_, (a, b))| a < b)
        .map(|(idx, _)| idx + 1)
        .sum();
    println!("01: {}", count);

    let messages = pairs
        .into_iter()
        .flat_map(|(x, y)| [x, y])
        .collect::<Vec<_>>();

    let first = Message::parse("[[2]]");
    let second = Message::parse("[[6]]");

    let first = 1 + messages.iter().filter(|e| **e < first).count();
    let second = 2 + messages.iter().filter(|e| **e < second).count();
    println!("02: {}", first * second);
}

#[cfg(test)]
mod tests {
    use crate::Message;
    use std::cmp::Ordering;

    #[test]
    fn examples_part01() {
        assert_eq!(
            Ordering::Less,
            Message::parse("[1,1,3,1,1]")
                .partial_cmp(&Message::parse("[1,1,5,1,1]"))
                .unwrap()
        );
        assert_eq!(
            Ordering::Less,
            Message::parse("[[1],[2,3,4]]")
                .partial_cmp(&Message::parse("[[1],4]"))
                .unwrap()
        );
        assert_eq!(
            Ordering::Greater,
            Message::parse("[9]")
                .partial_cmp(&Message::parse("[[8,7,6]]"))
                .unwrap()
        );
        assert_eq!(
            Ordering::Less,
            Message::parse("[[4,4],4,4]")
                .partial_cmp(&Message::parse("[[4,4],4,4,4]"))
                .unwrap()
        );
        assert_eq!(
            Ordering::Greater,
            Message::parse("[7,7,7,7]")
                .partial_cmp(&Message::parse("[7,7,7]"))
                .unwrap()
        );
        assert_eq!(
            Ordering::Less,
            Message::parse("[]")
                .partial_cmp(&Message::parse("[3]"))
                .unwrap()
        );
        assert_eq!(
            Ordering::Greater,
            Message::parse("[[[]]]")
                .partial_cmp(&Message::parse("[[]]"))
                .unwrap()
        );
        assert_eq!(
            Ordering::Greater,
            Message::parse("[1,[2,[3,[4,[5,6,7]]]],8,9]")
                .partial_cmp(&Message::parse("[1,[2,[3,[4,[5,6,0]]]],8,9]"))
                .unwrap()
        );
    }
}
