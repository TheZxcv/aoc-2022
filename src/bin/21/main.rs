use std::collections::HashMap;

#[derive(Copy, Clone)]
enum Operator {
    Add,
    Sub,
    Mul,
    Div,
}

impl Operator {
    fn apply(&self, left: i64, right: i64) -> i64 {
        match self {
            Self::Add => left + right,
            Self::Sub => left - right,
            Self::Mul => left * right,
            Self::Div => left / right,
        }
    }
}

enum Value {
    Number(i64),
    Expr(String, Operator, String),
    Missing(),
}

#[derive(Clone)]
enum Result {
    Any(),
    Number(i64),
    Expr(Box<Result>, Operator, Box<Result>),
}

impl Result {
    fn simplify(self) -> Self {
        match self {
            Self::Expr(left, op, right) => match (*left, *right) {
                (Self::Number(l), Self::Number(r)) => Self::Number(op.apply(l, r)),
                (left, right) => Self::Expr(Box::new(left), op, Box::new(right)),
            },
            _ => self,
        }
    }
}

impl Value {
    fn parse(line: &str) -> (String, Self) {
        let colon_idx = line.find(':').unwrap();
        let name = line[..colon_idx].to_string();

        let splits: Vec<_> = line[colon_idx + 2..].split_ascii_whitespace().collect();
        if splits.len() == 1 {
            (name, Self::Number(splits[0].parse().unwrap()))
        } else {
            let left = splits[0].parse().unwrap();
            let operator = Self::parse_operator(splits[1]);
            let right = splits[2].parse().unwrap();
            (name, Self::Expr(left, operator, right))
        }
    }

    fn parse_operator(s: &str) -> Operator {
        if s.len() != 1 {
            panic!("Invalid operator: {}", s);
        }

        match s.chars().nth(0).unwrap() {
            '+' => Operator::Add,
            '-' => Operator::Sub,
            '*' => Operator::Mul,
            '/' => Operator::Div,
            ch => panic!("Invalid operator: {}", ch),
        }
    }
}

struct Calculator {
    symbols: HashMap<String, Value>,
}

impl Calculator {
    fn new(values: Vec<(String, Value)>) -> Self {
        let mut symbols = HashMap::new();

        for (name, value) in values {
            symbols.insert(name, value);
        }

        Self { symbols }
    }

    fn eval(&self, name: &str) -> Result {
        match &self.symbols[name] {
            Value::Number(result) => Result::Number(*result),
            Value::Expr(left, operand, right) => {
                self.eval_expression(self.eval(&left), *operand, self.eval(&right))
            }
            Value::Missing() => Result::Any(),
        }
    }

    fn solve(&self, name: &str) -> i64 {
        match &self.symbols[name] {
            Value::Expr(left, _, right) => match (self.eval(&left), self.eval(&right)) {
                (Result::Number(value), expr) => self.solve_missing(expr, value),
                (expr, Result::Number(value)) => self.solve_missing(expr, value),
                _ => panic!("Cannot solved, missing known."),
            },
            _ => panic!("Must be an expression."),
        }
    }

    fn eval_expression(&self, left: Result, operator: Operator, right: Result) -> Result {
        Result::Expr(Box::new(left), operator, Box::new(right)).simplify()
    }

    fn solve_missing(&self, expr: Result, expected_result: i64) -> i64 {
        match expr {
            Result::Any() => expected_result,
            Result::Expr(left, operator, right) => match (*left, *right) {
                (Result::Number(value), expr) => {
                    self.solve_missing(expr, self.undo_right(value, operator, expected_result))
                }
                (expr, Result::Number(value)) => {
                    self.solve_missing(expr, self.undo_left(operator, value, expected_result))
                }
                _ => panic!("Cannot solved, missing known."),
            },
            Result::Number(_) => panic!("Cannot solve a number!"),
        }
    }

    fn undo_left(&self, operator: Operator, right: i64, result: i64) -> i64 {
        match operator {
            Operator::Add => result - right,
            Operator::Sub => result + right,
            Operator::Mul => result / right,
            Operator::Div => result * right,
        }
    }

    fn undo_right(&self, left: i64, operator: Operator, result: i64) -> i64 {
        match operator {
            Operator::Add => result - left,
            Operator::Sub => left - result,
            Operator::Mul => result / left,
            Operator::Div => left / result,
        }
    }

    fn set_value(&mut self, name: &str, value: Value) {
        self.symbols.insert(name.to_string(), value);
    }
}

fn main() {
    let lines = aoc_2022::input_lines();
    let values: Vec<_> = lines.iter().map(|line| Value::parse(line)).collect();
    let mut calculator = Calculator::new(values);
    let result = if let Result::Number(result) = calculator.eval("root") {
        result
    } else {
        panic!()
    };
    println!("01: {}", result);

    calculator.set_value("humn", Value::Missing());
    let result = calculator.solve("root");
    println!("02: {}", result);
}

#[cfg(test)]
mod tests {
    use crate::{Calculator, Result, Value};

    #[test]
    fn examples_part01() {
        let lines = [
            "root: pppw + sjmn",
            "dbpl: 5",
            "cczh: sllz + lgvd",
            "zczc: 2",
            "ptdq: humn - dvpt",
            "dvpt: 3",
            "lfqf: 4",
            "humn: 5",
            "ljgn: 2",
            "sjmn: drzm * dbpl",
            "sllz: 4",
            "pppw: cczh / lfqf",
            "lgvd: ljgn * ptdq",
            "drzm: hmdt - zczc",
            "hmdt: 32",
        ]
        .map(String::from);
        let values: Vec<_> = lines.iter().map(|line| Value::parse(line)).collect();

        let calculator = Calculator::new(values);
        assert!(matches!(calculator.eval("root"), Result::Number(152)));
    }

    #[test]
    fn examples_part02() {
        let lines = [
            "root: pppw + sjmn",
            "dbpl: 5",
            "cczh: sllz + lgvd",
            "zczc: 2",
            "ptdq: humn - dvpt",
            "dvpt: 3",
            "lfqf: 4",
            "humn: 5",
            "ljgn: 2",
            "sjmn: drzm * dbpl",
            "sllz: 4",
            "pppw: cczh / lfqf",
            "lgvd: ljgn * ptdq",
            "drzm: hmdt - zczc",
            "hmdt: 32",
        ]
        .map(String::from);
        let values: Vec<_> = lines.iter().map(|line| Value::parse(line)).collect();

        let mut calculator = Calculator::new(values);
        calculator.set_value("humn", Value::Missing());
        assert_eq!(301, calculator.solve("root"));
    }
}
