use std::collections::HashMap;

#[derive(Clone, Copy)]
enum Action {
    Left,
    Right,
}

impl Action {
    fn parse(ch: char) -> Self {
        match ch {
            '>' => Self::Right,
            '<' => Self::Left,
            _ => panic!(),
        }
    }
}

struct Game {
    height: usize,
    width: usize,
    highest_rock: usize,
    board: Vec<Vec<bool>>,
    next_action_idx: usize,
    actions: Vec<Action>,
}

impl Game {
    fn new(actions: Vec<Action>) -> Self {
        Self {
            highest_rock: 0,
            height: 100_000,
            width: 7,
            board: vec![vec![false; 7]; 100_000],
            next_action_idx: 0,
            actions,
        }
    }

    fn next_action(&mut self) -> Action {
        let action = self.actions[self.next_action_idx];
        self.next_action_idx = (self.next_action_idx + 1) % self.actions.len();

        action
    }

    fn drop(&mut self, shape: &Vec<Vec<bool>>) {
        let mut position = (2, self.highest_rock + 3 + (shape.len() - 1));

        loop {
            position = self.apply_action(shape, position);
            if !self.can_move_down(shape, position) {
                break;
            }

            position.1 -= 1;
        }

        self.highest_rock = usize::max(self.highest_rock, position.1 + 1);

        self.draw(shape, position);
    }

    fn can_move_down(&mut self, shape: &Vec<Vec<bool>>, position: (usize, usize)) -> bool {
        if position.1 == 0 {
            return false;
        }
        let new_position = (position.0, position.1 - 1);
        self.is_valid_position(shape, new_position)
    }

    fn apply_action(&mut self, shape: &Vec<Vec<bool>>, position: (usize, usize)) -> (usize, usize) {
        let action = self.next_action();
        if position.0 == 0 {
            if let Action::Left = action {
                return position;
            }
        }

        let px = match action {
            Action::Left => position.0 - 1,
            Action::Right => position.0 + 1,
        };

        let new_position = (px, position.1);
        if self.is_valid_position(shape, new_position) {
            new_position
        } else {
            position
        }
    }

    fn draw(&mut self, shape: &Vec<Vec<bool>>, position: (usize, usize)) {
        let bottom = position.1 - (shape.len() - 1);
        let left = position.0;

        for i in 0..shape.len() {
            for j in 0..shape[i].len() {
                let ii = bottom + i;
                let jj = left + j;
                if shape[i][j] {
                    assert!(!self.board[ii][jj]);
                    self.board[ii][jj] = true;
                }
            }
        }
    }

    fn is_valid_position(&mut self, shape: &Vec<Vec<bool>>, position: (usize, usize)) -> bool {
        if position.1 < shape.len() - 1 {
            return false;
        }

        let top = position.1;
        let bottom = position.1 - (shape.len() - 1);
        let left = position.0;
        let right = position.0 + shape[0].len() - 1;
        if right >= self.width || top >= self.height {
            return false;
        }

        for i in 0..shape.len() {
            for j in 0..shape[i].len() {
                let jj = left + j;
                let ii = bottom + i;
                if shape[i][j] && self.board[ii][jj] {
                    return false;
                }
            }
        }

        true
    }

    fn print(&self) {
        let profile = self.profile();
        let max_distance = profile.iter().max().unwrap();
        let start = if self.highest_rock - max_distance == 0 {
            0
        } else {
            self.highest_rock - max_distance - 1
        };
        for line in self.board[start..=self.highest_rock].iter().rev() {
            print!("#");
            for b in line {
                if *b {
                    print!("X");
                } else {
                    print!(" ");
                }
            }
            println!("#");
        }
        println!("#########");
    }

    fn profile(&self) -> [usize; 7] {
        let mut heights: [usize; 7] = [self.highest_rock; 7];
        for column in 0..self.width {
            for row in (0..self.highest_rock).rev() {
                if self.board[row][column] {
                    heights[column] -= row + 1;
                    break;
                }
            }
        }

        heights
    }
}

fn simulate_for(actions: &[Action], simulation_length: usize) -> u64 {
    let shapes: [Vec<Vec<bool>>; 5] = [
        // -
        vec![vec![true, true, true, true]],
        // +
        vec![
            vec![false, true, false],
            vec![true, true, true],
            vec![false, true, false],
        ],
        // mirrored L
        vec![
            vec![true, true, true],
            vec![false, false, true],
            vec![false, false, true],
        ],
        // |
        vec![vec![true], vec![true], vec![true], vec![true]],
        // o
        vec![vec![true, true], vec![true, true]],
    ];

    let mut states: HashMap<([usize; 7], usize, usize), (usize, usize)> = HashMap::new();
    let mut game = Game::new(actions.to_vec());
    let mut rocks_dropped: u64 = 0;
    let mut loop_length = 0;
    let mut loop_gain = 0;
    let mut next_shape_idx = 0;

    for i in 0..simulation_length {
        let shape = &shapes[next_shape_idx];
        let state = (game.profile(), next_shape_idx, game.next_action_idx);
        if states.contains_key(&state) {
            let (idx, prev_highest) = states.get(&state).unwrap();
            loop_gain = game.highest_rock - prev_highest;
            loop_length = i - idx;
            break;
        } else {
            states.insert(state, (i, game.highest_rock));
        }
        game.drop(shape);

        rocks_dropped += 1;
        next_shape_idx = (next_shape_idx + 1) % shapes.len();
    }

    if rocks_dropped as usize == simulation_length {
        return game.highest_rock as u64;
    }

    let rocks_to_drop = simulation_length as u64 - rocks_dropped;
    let repetition = rocks_to_drop / loop_length as u64;
    let remaining_rocks = rocks_to_drop % loop_length as u64;
    assert_eq!(
        simulation_length as u64,
        rocks_dropped + remaining_rocks + repetition * loop_length as u64
    );

    for _ in 0..remaining_rocks {
        let shape = &shapes[next_shape_idx];
        next_shape_idx = (next_shape_idx + 1) % shapes.len();
        game.drop(shape);
    }

    repetition * loop_gain as u64 + game.highest_rock as u64
}

fn main() {
    let lines = aoc_2022::input_lines();
    let actions = lines[0]
        .chars()
        .map(|ch| Action::parse(ch))
        .collect::<Vec<_>>();

    println!("01: {}", simulate_for(&actions, 2022));
    println!("02: {}", simulate_for(&actions, 1000000000000));
}

#[cfg(test)]
mod tests {
    use crate::{simulate_for, Action};

    #[test]
    fn examples_part01() {
        let actions = ">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>"
            .chars()
            .map(|ch| Action::parse(ch))
            .collect::<Vec<_>>();
        assert_eq!(3068, simulate_for(&actions, 2022));
    }

    #[test]
    fn examples_part02() {
        let actions = ">>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>"
            .chars()
            .map(|ch| Action::parse(ch))
            .collect::<Vec<_>>();
        assert_eq!(1514285714288, simulate_for(&actions, 1000000000000));
    }
}
