struct Blueprint {
    ore_cost: i32,
    clay_cost: i32,
    // Ore, Clay
    obsidian_cost: (i32, i32),
    // Ore, Obsidian
    geode_cost: (i32, i32),
}

impl Blueprint {
    fn parse(line: &str) -> Self {
        let ore_cost = {
            let marker = "Each ore robot costs ";
            let start_idx = line.find(marker).unwrap() + marker.len();
            let space_idx = start_idx + line[start_idx..].find(' ').unwrap();

            line[start_idx..space_idx].parse().unwrap()
        };

        let clay_cost = {
            let marker = "Each clay robot costs ";
            let start_idx = line.find(marker).unwrap() + marker.len();
            let space_idx = start_idx + line[start_idx..].find(' ').unwrap();

            line[start_idx..space_idx].parse().unwrap()
        };

        let obsidian_cost = {
            let marker = "Each obsidian robot costs ";
            let start_idx = line.find(marker).unwrap() + marker.len();
            let space_idx = start_idx + line[start_idx..].find(' ').unwrap();

            let ores = line[start_idx..space_idx].parse().unwrap();

            let marker2 = " ore and ";
            let start_idx = space_idx + marker2.len();
            let space_idx = start_idx + line[start_idx..].find(' ').unwrap();

            let clays = line[start_idx..space_idx].parse().unwrap();

            (ores, clays)
        };
        let geode_cost = {
            let marker = "Each geode robot costs ";
            let start_idx = line.find(marker).unwrap() + marker.len();
            let space_idx = start_idx + line[start_idx..].find(' ').unwrap();

            let ores = line[start_idx..space_idx].parse().unwrap();

            let marker2 = " ore and ";
            let start_idx = space_idx + marker2.len();
            let space_idx = start_idx + line[start_idx..].find(' ').unwrap();

            let obsidians = line[start_idx..space_idx].parse().unwrap();

            (ores, obsidians)
        };

        Self {
            ore_cost,
            clay_cost,
            obsidian_cost,
            geode_cost,
        }
    }
}

#[derive(Eq, PartialEq, Clone, Copy)]
enum RobotType {
    Ore,
    Clay,
    Obsidian,
    Geode,
}

#[derive(Clone, Copy)]
struct SimulationState {
    ore: i32,
    clay: i32,
    obsidian: i32,
    geode: i32,
    ore_robots: i32,
    clay_robots: i32,
    obsidian_robots: i32,
    geode_robots: i32,
}

struct GeodeCrackingSimulation {
    blueprint: Blueprint,
}

impl GeodeCrackingSimulation {
    fn new(blueprint: Blueprint) -> Self {
        Self { blueprint }
    }

    fn most_geode_within_time(&self, time: i32) -> i32 {
        let starting_state = SimulationState {
            ore: 0,
            clay: 0,
            obsidian: 0,
            geode: 0,
            ore_robots: 1,
            clay_robots: 0,
            obsidian_robots: 0,
            geode_robots: 0,
        };
        let most_geode_state = self.recursive_search(starting_state, time);

        most_geode_state.geode
    }

    fn recursive_search(&self, state: SimulationState, time: i32) -> SimulationState {
        if time == 0 {
            return state;
        } else if time == 1 {
            return self.run_robots(state);
        }

        let types = [
            RobotType::Geode,
            RobotType::Obsidian,
            RobotType::Clay,
            RobotType::Ore,
        ];
        let should_can_build = types.map(|r| {
            (
                self.should_build(r, &state, time),
                self.can_build(r, &state),
            )
        });

        let state = self.run_robots(state);

        should_can_build
            .iter()
            .enumerate()
            .filter(|(_, (should, _))| *should)
            .map(|(idx, (_, can))| {
                if *can {
                    self.recursive_search(self.build_robot(types[idx], &state), time - 1)
                } else {
                    self.recurse_with_chosen_type(state, time - 1, types[idx])
                }
            })
            .max_by_key(|s| s.geode)
            .unwrap()
    }

    fn recurse_with_chosen_type(
        &self,
        state: SimulationState,
        time: i32,
        want_to_build: RobotType,
    ) -> SimulationState {
        if time == 0 {
            return state;
        } else if time == 1 {
            return self.run_robots(state);
        }

        let can_build = self.can_build(want_to_build, &state);

        let state = self.run_robots(state);

        if can_build {
            self.recursive_search(self.build_robot(want_to_build, &state), time - 1)
        } else {
            self.recurse_with_chosen_type(state, time - 1, want_to_build)
        }
    }

    fn can_build(&self, robot_type: RobotType, state: &SimulationState) -> bool {
        match robot_type {
            RobotType::Ore => state.ore >= self.blueprint.ore_cost,
            RobotType::Clay => state.ore >= self.blueprint.clay_cost,
            RobotType::Obsidian => {
                state.ore >= self.blueprint.obsidian_cost.0
                    && state.clay >= self.blueprint.obsidian_cost.1
            }
            RobotType::Geode => {
                state.ore >= self.blueprint.geode_cost.0
                    && state.obsidian >= self.blueprint.geode_cost.1
            }
        }
    }

    fn should_build(&self, robot_type: RobotType, state: &SimulationState, time: i32) -> bool {
        if time <= 1 {
            return false;
        }

        match robot_type {
            RobotType::Ore => {
                let max_required = i32::max(
                    i32::max(self.blueprint.ore_cost, self.blueprint.clay_cost),
                    i32::max(self.blueprint.obsidian_cost.0, self.blueprint.geode_cost.0),
                );
                state.ore_robots < max_required
                    && (state.ore_robots * time + state.ore) / time < max_required
            }
            RobotType::Clay => {
                state.clay_robots < self.blueprint.obsidian_cost.1
                    && (state.clay_robots * time + state.clay) / time
                        < self.blueprint.obsidian_cost.1
            }
            RobotType::Obsidian => {
                state.obsidian_robots < self.blueprint.geode_cost.1
                    && (state.obsidian_robots * time + state.obsidian) / time
                        < self.blueprint.geode_cost.1
            }
            RobotType::Geode => true,
        }
    }
    fn run_robots(&self, state: SimulationState) -> SimulationState {
        let mut new_state = state;

        new_state.ore += state.ore_robots;
        new_state.clay += state.clay_robots;
        new_state.obsidian += state.obsidian_robots;
        new_state.geode += state.geode_robots;

        new_state
    }

    fn build_robot(&self, robot_type: RobotType, state: &SimulationState) -> SimulationState {
        let mut new_state = state.clone();

        match robot_type {
            RobotType::Ore => {
                new_state.ore -= self.blueprint.ore_cost;
                new_state.ore_robots += 1;
            }
            RobotType::Clay => {
                new_state.ore -= self.blueprint.clay_cost;
                new_state.clay_robots += 1;
            }
            RobotType::Obsidian => {
                new_state.ore -= self.blueprint.obsidian_cost.0;
                new_state.clay -= self.blueprint.obsidian_cost.1;
                new_state.obsidian_robots += 1;
            }
            RobotType::Geode => {
                new_state.ore -= self.blueprint.geode_cost.0;
                new_state.obsidian -= self.blueprint.geode_cost.1;
                new_state.geode_robots += 1
            }
        }

        new_state
    }
}

fn main() {
    let lines = aoc_2022::input_lines();

    let simulations = lines
        .into_iter()
        .map(|line| Blueprint::parse(&line))
        .map(|b| GeodeCrackingSimulation::new(b))
        .collect::<Vec<_>>();

    let quality_levels_sum: u64 = simulations
        .iter()
        .map(|s| s.most_geode_within_time(24))
        .enumerate()
        .map(|(idx, value)| (idx + 1) as u64 * value as u64)
        .sum();
    println!("01: {}", quality_levels_sum);

    let product = simulations
        .iter()
        .take(3)
        .map(|s| s.most_geode_within_time(32))
        .fold(1, |acc, el| el as u64 * acc);
    println!("02: {}", product);
}

#[cfg(test)]
mod tests {
    use crate::{Blueprint, GeodeCrackingSimulation};

    #[test]
    fn examples_part01() {
        let blueprints = [
            Blueprint {
                ore_cost: 4,
                clay_cost: 2,
                obsidian_cost: (3, 14),
                geode_cost: (2, 7),
            },
            Blueprint {
                ore_cost: 2,
                clay_cost: 3,
                obsidian_cost: (3, 8),
                geode_cost: (3, 12),
            },
        ];

        let geodes = blueprints
            .map(|b| GeodeCrackingSimulation::new(b))
            .map(|s| s.most_geode_within_time(24));

        assert_eq!(9, geodes[0]);
        assert_eq!(12, geodes[1]);
    }

    #[test]
    fn examples_part02() {
        let blueprints = [
            Blueprint {
                ore_cost: 4,
                clay_cost: 2,
                obsidian_cost: (3, 14),
                geode_cost: (2, 7),
            },
            Blueprint {
                ore_cost: 2,
                clay_cost: 3,
                obsidian_cost: (3, 8),
                geode_cost: (3, 12),
            },
        ];

        let geodes = blueprints
            .map(|b| GeodeCrackingSimulation::new(b))
            .map(|s| s.most_geode_within_time(32));

        assert_eq!(56, geodes[0]);
        assert_eq!(62, geodes[1]);
    }
}
