use std::{collections::HashMap, iter::Peekable};

enum Node {
    File(String, usize),
    Directory(String, Box<HashMap<String, Node>>),
}

impl Node {
    fn file(name: &str, size: usize) -> Node {
        Node::File(name.to_string(), size)
    }

    fn directory(name: &str, files: HashMap<String, Node>) -> Node {
        Node::Directory(name.to_string(), Box::new(files))
    }

    fn get_directory_sizes(&self) -> Vec<usize> {
        let mut sizes = Vec::new();
        self.get_directory_sizes_rec(&mut sizes);
        sizes
    }

    fn get_directory_sizes_rec(&self, sizes: &mut Vec<usize>) -> usize {
        match self {
            Self::File(_, size) => *size,
            Self::Directory(_, content) => {
                let size = content
                    .values()
                    .map(|n| n.get_directory_sizes_rec(sizes))
                    .sum();

                sizes.push(size);

                size
            }
        }
    }

    fn parse_from_root(lines: &[String]) -> Node {
        let mut iter = lines.iter().peekable();
        assert_eq!("$ cd /", iter.next().unwrap());

        let nodes = Self::parse_directory(&mut iter);
        Self::directory("/", nodes)
    }

    fn parse_directory<'a>(
        iter: &mut Peekable<impl Iterator<Item = &'a String>>,
    ) -> HashMap<String, Node> {
        let mut nodes: HashMap<String, Node> = HashMap::new();
        while let Some(cmd) = iter.next() {
            if cmd.starts_with("$ cd ..") {
                break;
            } else if cmd.starts_with("$ cd ") {
                let name = &cmd[5..];
                let content = Self::parse_directory(iter);
                if let Some(Self::Directory(_, old_content)) = nodes.get_mut(name) {
                    old_content.extend(content);
                } else {
                    nodes.insert(name.to_string(), Self::directory(name, content));
                }
            } else if cmd.starts_with("$ ls") {
                while let Some(line) = iter.peek() {
                    if line.starts_with("$") {
                        break;
                    }

                    let line = iter.next().unwrap();

                    if line.starts_with("dir ") {
                        let name = &line[4..];
                        if !nodes.contains_key(name) {
                            nodes.insert(name.to_string(), Self::directory(name, HashMap::new()));
                        }
                    } else {
                        let mut splits = line.split_ascii_whitespace();
                        let size: usize = splits.next().unwrap().parse().unwrap();
                        let name = splits.next().unwrap();
                        nodes.insert(name.to_string(), Self::file(name, size));
                    }
                }
            } else {
                panic!("Unknown command: {}", cmd);
            }
        }

        nodes
    }
}

fn main() {
    let lines = aoc_2022::input_lines();
    let tree = Node::parse_from_root(&lines);
    let dir_sizes = tree.get_directory_sizes();

    let capped_size: usize = dir_sizes.iter().filter(|&&s| s <= 100000).sum();
    println!("01: {}", capped_size);

    // The last one is the size of the root folder.
    let total_size = *dir_sizes.last().unwrap();
    let to_free = 30000000 - (70000000 - total_size);
    let smallest = dir_sizes
        .into_iter()
        .filter(|&s| s >= to_free)
        .min()
        .unwrap();
    println!("02: {}", smallest);
}

#[cfg(test)]
mod tests {
    use crate::Node;

    #[test]
    fn examples_part01() {
        let input = [
            "$ cd /",
            "$ ls",
            "dir a",
            "14848514 b.txt",
            "8504156 c.dat",
            "dir d",
            "$ cd a",
            "$ ls",
            "dir e",
            "29116 f",
            "2557 g",
            "62596 h.lst",
            "$ cd e",
            "$ ls",
            "584 i",
            "$ cd ..",
            "$ cd ..",
            "$ cd d",
            "$ ls",
            "4060174 j",
            "8033020 d.log",
            "5626152 d.ext",
            "7214296 k",
        ]
        .map(String::from);

        let tree = Node::parse_from_root(&input);
        let sizes = tree.get_directory_sizes();

        let capped_size: usize = sizes.into_iter().filter(|&s| s <= 100000).sum();
        assert_eq!(95437, capped_size);
    }

    #[test]
    fn examples_part02() {
        let input = [
            "$ cd /",
            "$ ls",
            "dir a",
            "14848514 b.txt",
            "8504156 c.dat",
            "dir d",
            "$ cd a",
            "$ ls",
            "dir e",
            "29116 f",
            "2557 g",
            "62596 h.lst",
            "$ cd e",
            "$ ls",
            "584 i",
            "$ cd ..",
            "$ cd ..",
            "$ cd d",
            "$ ls",
            "4060174 j",
            "8033020 d.log",
            "5626152 d.ext",
            "7214296 k",
        ]
        .map(String::from);

        let tree = Node::parse_from_root(&input);
        let sizes = tree.get_directory_sizes();

        let total_size = *sizes.last().unwrap();
        let to_free = 30000000 - (70000000 - total_size);
        let smallest = sizes.into_iter().filter(|&s| s >= to_free).min().unwrap();
        assert_eq!(24933642, smallest);
    }
}
