use std::str::Chars;

fn has_duplicates(buffer: &Vec<char>) -> bool {
    for i in 0..buffer.len() {
        for j in (i + 1)..buffer.len() {
            if buffer[i] == buffer[j] {
                return true;
            }
        }
    }

    false
}

fn find_sequence_of_uniques(mut chars: Chars, length: usize) -> usize {
    let mut buffer = vec!['\0'; length];
    let mut read = length;
    for i in 0..length {
        buffer[i] = chars.next().unwrap();
    }

    if !has_duplicates(&buffer) {
        return read;
    }

    let mut index = 0;
    for char in chars {
        buffer[index] = char;
        read += 1;

        if !has_duplicates(&buffer) {
            break;
        }

        index = (index + 1) % length;
    }

    return read;
}

fn main() {
    let lines = aoc_2022::input_lines();
    let signal = lines.first().unwrap();

    let read = find_sequence_of_uniques(signal.chars(), 4);
    println!("01: {}", read);

    let read = find_sequence_of_uniques(signal.chars(), 14);
    println!("02: {}", read);
}

#[cfg(test)]
mod tests {
    use crate::find_sequence_of_uniques;

    #[test]
    fn examples_part01() {
        let cases = [
            ("bvwbjplbgvbhsrlpgdmjqwftvncz", 5),
            ("nppdvjthqldpwncqszvftbrmjlhg", 6),
            ("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 10),
            ("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 11),
        ];

        for (input, expected) in cases {
            assert_eq!(find_sequence_of_uniques(input.chars(), 4), expected);
        }
    }

    #[test]
    fn examples_part02() {
        let cases = [
            ("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 19),
            ("bvwbjplbgvbhsrlpgdmjqwftvncz", 23),
            ("nppdvjthqldpwncqszvftbrmjlhg", 23),
            ("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 29),
            ("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 26),
        ];

        for (input, expected) in cases {
            assert_eq!(find_sequence_of_uniques(input.chars(), 14), expected);
        }
    }
}
