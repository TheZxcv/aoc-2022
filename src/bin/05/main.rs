#[derive(Clone)]
struct Stacks {
    stacks: Vec<Vec<char>>,
}

impl Stacks {
    fn parse(layout: &[String]) -> Self {
        let columns_cnt = layout
            .iter()
            .last()
            .unwrap()
            .split_ascii_whitespace()
            .count();
        let mut stacks = vec![vec![]; columns_cnt];

        for line in layout.iter().rev().skip(1) {
            for i in 0..columns_cnt {
                let index = (4 * i + 1) as usize;
                if let Some(ch) = line.chars().nth(index) {
                    if ch.is_alphabetic() {
                        stacks[i].push(ch);
                    }
                }
            }
        }

        Self { stacks }
    }

    fn tops(&self) -> String {
        self.stacks
            .iter()
            .filter_map(|v| v.last())
            .collect::<String>()
    }
}

struct Action {
    from: usize,
    to: usize,
    count: i32,
}

impl Action {
    fn parse(line: &str) -> Self {
        assert!(line.starts_with("move "));
        let mut splits = line.split_ascii_whitespace();
        assert!(splits.next() == Some("move"));

        let count: i32 = splits.next().unwrap().parse().unwrap();

        assert!(splits.next() == Some("from"));
        let from: usize = splits.next().unwrap().parse().unwrap();

        assert!(splits.next() == Some("to"));
        let to: usize = splits.next().unwrap().parse().unwrap();

        Self { from, to, count }
    }

    fn apply(&self, mut stacks: Stacks) -> Stacks {
        for _ in 0..self.count {
            let top = stacks.stacks[self.from - 1].pop();
            if let Some(value) = top {
                stacks.stacks[self.to - 1].push(value);
            } else {
                break;
            }
        }

        stacks
    }

    fn apply_retain_order(&self, mut stacks: Stacks) -> Stacks {
        let mut temp = Vec::new();
        for _ in 0..self.count {
            let top = stacks.stacks[self.from - 1].pop();
            if let Some(value) = top {
                temp.push(value);
            } else {
                break;
            }
        }

        for el in temp.into_iter().rev() {
            stacks.stacks[self.to - 1].push(el);
        }

        stacks
    }
}

fn main() {
    let lines = aoc_2022::input_lines();

    let empty_line_idx = lines.iter().position(|line| line.is_empty()).unwrap();
    let (stacks_layout, actions) = lines.split_at(empty_line_idx);

    let stacks = Stacks::parse(stacks_layout);
    let actions = actions
        .iter()
        .skip(1) // Skip blank line
        .map(|line| Action::parse(&line))
        .collect::<Vec<_>>();

    let final_state = actions
        .iter()
        .fold(stacks.clone(), |stacks, action| action.apply(stacks));
    println!("01: {}", final_state.tops());

    let final_state = actions.iter().fold(stacks.clone(), |stacks, action| {
        action.apply_retain_order(stacks)
    });
    println!("02: {}", final_state.tops());
}
